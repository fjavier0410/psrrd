<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<title>Login</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<h1>Ingresa tus credenciales.</h1>
			<form method="post" action="j_spring_security_check" role="login">
				<div class="form-group">
					<label>Ingresa tu RFC:</label> <input type="text" name="j_username"
						required class="form-control" /> <a href="#">Recordar E-Mail</a>
				</div>

				<div class="form-group">
					<label>Ingresa tu Password:</label> <input type="password"
						name="j_password" required class="form-control" />

				</div>
				<font color="red"> <span>${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</span>
				</font>
				<button type="submit" name="go" class="btn btn-primary">Ingresar</button>

			</form>
			  <a href="<c:url value="/recuperacionpass"/>" >Olvide mi contraseņa</a>
		</div>
		


 </div>

 
</body>
</html>