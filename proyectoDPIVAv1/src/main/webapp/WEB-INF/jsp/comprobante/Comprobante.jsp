<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<div class="container">
		<div class="stepwizard col-md-offset-3">
			<div class="stepwizard-row setup-panel">
				<div class="stepwizard-step">
					<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
					<p>PASO 1</p>
				</div>
				<div class="stepwizard-step">
					<a href="#step-2" type="button" class="btn btn-default btn-circle"
						disabled="disabled">2</a>
					<p>PASO 2</p>
				</div>
				<div class="stepwizard-step">
					<a href="#step-3" type="button" class="btn btn-default btn-circle"
						disabled="disabled">3</a>
					<p>PASO 3</p>
				</div>
				<div class="stepwizard-step">
					<a href="#step-4" type="button" class="btn btn-default btn-circle"
						disabled="disabled">4</a>
					<p>PASO 4</p>
				</div>
			</div>
		</div>
		<form:form role="form" action="comprobante.do" method="post" commandName="comprobante">
			<div class="row setup-content" id="step-1">
				<div class="col-xs-6 col-md-offset-3">
					<div class="col-md-12">
						<h3>DATOS DE IDENTIFICACI�N</h3>
						<div class="form-group">
							<label class="control-label">Registro Federal de
								Contribuyentes:</label> <input maxlength="100" type="text"
								required="required" class="form-control"
								placeholder="Ingrese su RFC" />
						</div>
						<div class="form-group">
							<label class="control-label">CURP:</label> <input maxlength="100"
								type="text" required="required" class="form-control"
								placeholder="Ingrese su CURP" />
						</div>
						<div class="form-group">
							<label class="control-label">Apellido Paterno:</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Apellido Paterno" />
						</div>
						<div class="form-group">
							<label class="control-label">Apellido Materno:</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Apellido Materno" />
						</div>
						<div class="form-group">
							<label class="control-label">Nombre(s):</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Nombre(s)" />
						</div>
						<div class="form-group">
							<label class="control-label">Denominaci�n o Raz�n Social</label>
							<textarea required="required" class="form-control"
								placeholder="Raz�n social"></textarea>
						</div>
						<button class="btn btn-primary nextBtn btn-lg pull-right"
							type="button">Siguiente</button>
					</div>
				</div>
			</div>
			<div class="row setup-content" id="step-2">
				<div class="col-xs-6 col-md-offset-3">
					<div class="col-md-12">
						<h3>DATOS GENERALES</h3>
						<div class="form-group">
							<label class="control-label">Declaraci�n Informativa de
								Operaciones con Terceros</label>
						 	<form:select path="operacionesTerceros" onchange=""
								class="form-control" style="width: 400px;">
								<form:option value="NONE" label="--- Select ---" />
								<form:options items="${operacionesTercerosList}" />
							</form:select> 
						</div>
						<div class="form-group">
							<label class="control-label">Tipo de declaraci�n</label>
							<form:select path="tipoDeclaracion" class="form-control"
								style="width: 400px;">
								<form:option value="NONE" label="--- Select ---" />
								<form:options items="${tipoDeclaracionList}" />
							</form:select>
						</div>
						<div class="form-group">
							<label class="control-label">N�mero de Operaci�n o Folio
								Anterior</label> <input type="text" required="required"
								class="form-control" placeholder="Folio Anterior" />
						</div>
						<div class="form-group">
							<label class="control-label">Fecha de presentaci�n
								Anterior</label> <span class="add-on"> <i
								data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
							</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">Tipo de Periodicidad</label>
						<form:select path="tipoPeriodicidad"
							onchange="AldatuC('bat','bi')" name="bat" id="bat"
							class="form-control" style="width: 400px;">
							<form:option value="NONE" label="--- Select ---" />
					 		<form:options items="${tipoPeriodicidadList}" />
						</form:select>
					</div>
					<div class="form-group">
						<label class="control-label">Periodo</label>
						<form:select path="periodoMensual" name="bi" id="bi"
							class="form-control" style="width: 400px;">
							<form:option value="NONE" label="--- Select ---" />
						</form:select>
					</div>
					<button class="btn btn-primary nextBtn btn-lg pull-right"
						type="button">Siguiente</button>
				</div>
			</div>

  			<div class="row setup-content" id="step-3">
				<div class="col-xs-6 col-md-offset-3">
					<div class="col-md-12">
						<h3>INFORMACI�N DE IDENTIFICACI�N DEL PROVEEDOR O TERCERO</h3>
						<div class="form-group">
							<label class="control-label">Tipo de tercero</label>
							<form:select path="tipoDeclaracion" class="form-control"
								style="width: 400px;">
								<form:option value="NONE" label="--- Select ---" />
								<form:options items="${tipoDeclaracionList}" />
							</form:select>
						</div>
						<div class="form-group">
							<label class="control-label">Tipo de operaci�n</label>
							<form:select path="tipoDeclaracion" class="form-control"
								style="width: 400px;">
								<form:option value="NONE" label="--- Select ---" />
								<form:options items="${tipoDeclaracionList}" />
							</form:select>
						</div>
						<div class="form-group">
							<label class="control-label">RFC</label> <input type="text"
								required="required" class="form-control" placeholder="rfc" />
						</div>
						<div class="form-group">
							<label class="control-label">N�mero de ID Fiscal</label> <input
								type="text" required="required" class="form-control"
								placeholder="idFiscal" />
						</div>
						<div class="form-group">
							<label class="control-label">Nombre del Extranjero</label> <input
								type="text" required="required" class="form-control"
								placeholder="nombreExtranjero" />
						</div>
						<div class="form-group">
							<label class="control-label">Pa�s de residencia</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Nacionalidad</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<h3>INFORMACI�N DEL IMPUESTO AGREGADO</h3>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados a la tasa del 15% � del 16% de IVA</label> <input
								type="text" required="required" class="form-control"
								placeholder="valorActosActividadesTasa15o16IVA" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados a la tasa del 15% de IVA</label> <input type="text"
								required="required" class="form-control"
								placeholder="valorActosActividadesTasa15IVA" />
						</div>
						<div class="form-group">
							<label class="control-label">Monto del IVA pagado no
								acreditable a la tasa del 15% � 16% (correspondiente en la
								proporci�n de las deducciones autorizadas)</label> <input type="text"
								required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados a la tasa del 10% u 11% de IVA</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados a la tasa del 10% de IVA</label> <input type="text"
								required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Monto del IVA pagado no
								acreditable por la importaci�n a la tasa del 15% u 16%
								(correspondiente en la proporci�n de las deducciones
								autorizadas)</label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados en la importaci�n de bienes y servicios a la
								tasa del 15% � 16% de IVA</label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Monto del IVA pagado no
								acreditable por la importaci�n a la tasa del 15% � 16%
								(correspondiente en la proporci�n de las deducciones
								autorizadas)</label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados en la importaci�n de bienes y servicios a la
								tasa del 10% u 11% de IVA</label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Monto del IVA pagado no
								acreditable por la importanci�n a la tasa del 10% u 11% de IVA</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Monto del IVA pagado no
								acreditable por la importaci�n a la tasa del 10% u 11%
								(correspondiente en la proporci�n de las deducciones
								autorizadas) </label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados a la tasa del 0% de IVA </label> <input type="text"
								required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">Valor de los actos o
								actividades pagados por los que no se pagar� el IVA (Exentos)</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">IVA Retenido por el
								contribuyente</label> <input type="text" required="required"
								class="form-control" placeholder="paisResidencia" />
						</div>
						<div class="form-group">
							<label class="control-label">IVA correspondiente a las
								devoluciones, descuentos y bonificaciones sobre compras</label> <input
								type="text" required="required" class="form-control"
								placeholder="paisResidencia" />
						</div>
						<button class="btn btn-primary nextBtn btn-lg pull-right"
							type="button">Siguiente</button>
					</div>
				</div>
			</div>
	 	<div class="row setup-content" id="step-4">
				<div class="col-xs-6 col-md-offset-3">
					<div class="col-md-12">
						<h3>TOTALES</h3>
						<div class="form-group">
							<label class="control-label">Registro Federal de
								Contribuyentes:</label> <input maxlength="100" type="text"
								required="required" class="form-control"
								placeholder="Ingrese su RFC" />
						</div>
						<div class="form-group">
							<label class="control-label">CURP:</label> <input maxlength="100"
								type="text" required="required" class="form-control"
								placeholder="Ingrese su CURP" />
						</div>
						<div class="form-group">
							<label class="control-label">Apellido Paterno:</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Apellido Paterno" />
						</div>
						<div class="form-group">
							<label class="control-label">Apellido Materno:</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Apellido Materno" />
						</div>
						<div class="form-group">
							<label class="control-label">Nombre(s):</label> <input
								maxlength="100" type="text" required="required"
								class="form-control" placeholder="Nombre(s)" />
						</div>
						<div class="form-group">
							<label class="control-label">Denominaci�n o Raz�n Social</label>
							<textarea required="required" class="form-control"
								placeholder="Raz�n social"></textarea>
						</div>

						<button class="btn btn-success btn-lg pull-right" type="submit"
							name="action" value="compt">Submit</button>

					</div>
				</div>
			</div>
		</form:form>
	</div>


	<script
		src="<%=request.getContextPath()%>/resources/theme1/js/jquery.js"></script>

	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/theme1/js/jquery.min.js">
		
	</script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/theme1/js/bootstrap.min.js">
		
	</script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/theme1/js/bootstrap-datetimepicker.min.js">
		
	</script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/theme1/js/bootstrap-datetimepicker.mx-MX.js">
		
	</script>
	<script type="text/javascript">
		$('#datetimepicker').datetimepicker({
			format : 'dd/MM/yyyy hh:mm:ss',
			language : 'pt-BR'
		});
	</script>
</body>
<script type="text/javascript">
	function KargaC() {
		var s1 = document.getElementById('bat');
		var s2 = document.getElementById('bi');
		s1.options[0] = new Option("Bilbo", "0", "0");
		s1.options[1] = new Option("Bermio", "1", "1");
		s1.selectedIndex = 0;
		s2.options[0] = new Option("Moyua", "0", "0");
		s2.options[1] = new Option("Abando", "1", "1");
		s2.selectedIndex = 0;
	}
	function AldatuC(uan, txu) {
		var s1 = document.getElementById(uan);
		var s2 = document.getElementById(txu);

		alert(s1)
		alert(s2)
		s2.options.length = 0;
		if (s1.selectedIndex != 0) {
			if (s1.selectedIndex == 1) {
				s2.options[0] = new Option("Enero", "0", "0");
				s2.options[1] = new Option("Febrero", "1", "1");
				s2.options[2] = new Option("Marzo", "2", "2");
				s2.options[3] = new Option("Abril", "3", "3");
				s2.options[4] = new Option("Mayo", "4", "4");
				s2.options[5] = new Option("Junio", "5", "5");
				s2.options[6] = new Option("Julio", "6", "6");
				s2.options[7] = new Option("Agosto", "7", "7");
				s2.options[8] = new Option("Septiembre", "8", "8");
				s2.options[9] = new Option("Octubre", "9", "9");
				s2.options[10] = new Option("Noviembre", "10", "10");
				s2.options[11] = new Option("Diciembre", "11", "11");
				s2.selectedIndex = 0;
			}
			if (s1.selectedIndex == 2) {
				s2.options[0] = new Option("Enero - Febrero", "0", "0");
				s2.options[1] = new Option("Marzo - Abril", "1", "1");
				s2.options[2] = new Option("Mayo - Junio", "2", "2");
				s2.options[3] = new Option("Julio - Agosto", "3", "3");
				s2.options[4] = new Option("Septimbre - Octubre", "4", "4");
				s2.options[5] = new Option("Noviembre - Diciembre", "5", "5");
				s2.selectedIndex = 0;
			}
			if (s1.selectedIndex == 3) {
				s2.options[0] = new Option("Enero - Marzo", "0", "0");
				s2.options[1] = new Option("Abril - Junio", "1", "1");
				s2.options[2] = new Option("Julio - Septiembre", "2", "2");
				s2.options[3] = new Option("Octubre - Diciembre", "3", "3");
				s2.selectedIndex = 0;
			}
			if (s1.selectedIndex == 4) {
				s2.options[0] = new Option("Enero - Abril", "0", "0");
				s2.options[1] = new Option("Mayo - Agosto", "1", "1");
				s2.options[2] = new Option("Semptiembre - Diciembre", "2", "2");
				s2.selectedIndex = 0;
			}
			if (s1.selectedIndex == 5) {
				s2.options[0] = new Option("Enero - Junio", "0", "0");
				s2.options[1] = new Option("Julio - Diciembre", "1", "1");
				s2.selectedIndex = 0;
			}
		}
	}

	$(function() {
		$('#datetimepicker4').datetimepicker({
			pickTime : false
		});
	});
</script>

</html>