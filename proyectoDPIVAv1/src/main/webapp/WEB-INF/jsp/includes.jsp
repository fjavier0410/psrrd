<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>


<link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/styles.css" />
<link type="text/css" href="<%=request.getContextPath() %>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

<link type="text/css" href="<%=request.getContextPath()%>/resources/css/style.css" rel="stylesheet" />

<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script src="<%=request.getContextPath()%>/resources/js/script.js""></script>


<link href="<%=request.getContextPath() %>/resources/css/bootstrap-combined.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen"
     href="<%=request.getContextPath() %>/resources/css/bootstrap-datetimepicker.min.css">
     
     

<!-- This contains the most used tag libraries -->
