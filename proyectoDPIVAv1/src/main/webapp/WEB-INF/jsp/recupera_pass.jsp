<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alta Emisor</title>
</head>

<body>


<div class="jumbotron animated bounceInRight" >
    <div class="container">

        <div class="center-block">
            <h1 class="font-pacific letra-grande center-block">¿Olvidaste tu contraseña?</h1>
            <div class="page-header">
                <h2> Recupera tu contraseña </h2>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="panel panel-quadrum inicio text-center">
        <div class="panel-body fondoBlanco">
            <h3>Ingresa tu RFC para recuperar tu contraseña </h3>
            <form:form method="POST"  commandName="recuperaPass" >
                <input  name="erfc" type="text" class="form-control" required /> 
          		<input class="form-control" type="submit" value="Enviar"/>  
            
            </form:form>
          

        </div>
    </div>
</div>


</body>

</html>
