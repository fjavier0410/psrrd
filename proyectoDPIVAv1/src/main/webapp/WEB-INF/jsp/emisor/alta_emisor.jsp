<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alta Emisor</title>
</head>
<body>


  <div class="container">
    <div class="page-header">
    
    </div>
    <div class="row">
    
      <form:form method="POST" enctype="multipart/form-data" commandName="emisorRegister">
      
         <table class="table table-striped table-hover">
          
           <tr> 
             <td><form:label path="erfc">RFC* </form:label> </td>
             <td><form:input path="erfc"  /> </td>
            
             <td><form:label path="ecurp">CURP:  </form:label> </td>
             <td><form:input path="ecurp" /> </td>
             
            
             
             <td><form:label path="enombre">Nombre:  </form:label> </td>
             <td><form:input path="enombre"  /> </td>
             
             <td><form:label path="eapellpat">Apellido Paterno </form:label> </td>
             <td><form:input path="eapellpat"  /> </td>
           </tr>
         	
         	
         	 <tr> 
             
            
             <td><form:label path="eapellmat">Apellido Materno:  </form:label> </td>
             <td><form:input path="eapellmat"  /> </td>
             
             <td><form:label path="edenrazsoc">Razon Social:  </form:label> </td>
             <td><form:input path="edenrazsoc"  /> </td>
             
             <td><form:label path="ecorreoe">Correo:  </form:label> </td>
             <td><form:input path="ecorreoe"  /> </td>
             
             <td><form:label path="enumtel"> Telefono</form:label> </td>
             <td><form:input path="enumtel"  /> </td>
           </tr>
         
             <tr>
             <td> <form:input path="fileCer"  type="file" /> </td>
             <td> <form:input path="fileKey"  type="file" /> </td>
         	  <td><form:label  path="passwordKey"> Contraseņa:</form:label> </td>
             <td><form:input  path="passwordKey" type="password"  /> </td>
              
             </tr>
            
           <tr>
 				<td><input class="form-control" type="submit" value="Enviar"/>  </td>
			</tr>         
         </table>
       <form:errors path="*" element="div" cssClass="alert-danger"/>
      </form:form>
    
    </div>
  
  </div>

</body>
</html>