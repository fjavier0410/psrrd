<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admininistración de Emisores.</title>
</head>
<body>
	<h1>Datos del emisor</h1>
	<sec:authorize access="hasRole('Admin')">
		<form:form action="emisor.do" method="POST" commandName="emisor">
			<table>
				<tr>
					<td>ID Emisor</td>
					<td><form:input path="idEMISOR" /></td>
				</tr>
				<tr>
					<td>RFC</td>
					<td><form:input path="erfc" /></td>
				</tr>
				<tr>
					<td>CURP</td>
					<td><form:input path="ecurp" /></td>
				</tr>
				<tr>
					<td>Nombre</td>
					<td><form:input path="enombre" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="action" value="Agregar" />
						<input type="submit" name="action" value="Actualizar" /> <input
						type="submit" name="action" value="Eliminar" /> <input type="submit"
						name="action" value="Buscar" /></td>
				</tr>
			</table>
		</form:form>
	</sec:authorize>

	<br>
	<table border="1">
		<th>ID Emisor</th>
		<th>RFC</th>
		<th>CURP</th>
		<th>Nombre</th>
		<c:forEach items="${emisorList}" var="emisor">
			<tr>
				<td>${emisor.idEMISOR}</td>
				<td>${emisor.erfc}</td>
				<td>${emisor.ecurp}</td>
				<td>${emisor.enombre}</td>
			</tr>

		</c:forEach>
	</table>


</body>
</html>