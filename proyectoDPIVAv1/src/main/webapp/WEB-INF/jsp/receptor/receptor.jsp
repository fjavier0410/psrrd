<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admininistraci�n de Receptores.</title>
</head>
<body>
	

	<h1>Datos del receptor</h1>
	<sec:authorize access="hasRole('Admin')">
		<form:form action="receptor.do" method="POST" commandName="receptor">
			<table>
				<tr>
					<td>ID Receptor</td>
					<td><form:input path="idRECEPTOR" /></td>
				</tr>
				<tr>
					<td>RFC Receptor</td>
					<td><form:input path="rrfc" /></td>
				</tr>
				<tr>
					<td>CURP:</td>
					<td><form:input path="rcurp" /></td>
				</tr>
				<tr>
					<td>Nombre</td>
					<td><form:input path="rnombre" /></td>
				</tr>
				<tr>
					<td>Apellido Paterno</td>
					<td><form:input path="rapellpat" /></td>
				</tr>
				
				<tr>
					<td>Apellido Materno</td>
					<td><form:input path="rapellmat" /></td>
				</tr>

				<tr>
					<td>Raz�n Social</td>
					<td><form:input path="rdenrazsoc" /></td>
				</tr>
				
				<tr>
					<td>Correo Electr�nico</td>
					<td><form:input path="rcorreoe" /></td>
				</tr>
				
				<tr>
					<td>Numero de telefono</td>
					<td><form:input path="rnumtel" /></td>
				</tr>
				
				
				<tr>
					<td colspan="2"><input type="submit" name="action" value="Add" />
						<input type="submit" name="action" value="Edit" /> <input
						type="submit" name="action" value="Delete" /> <input type="submit"
						name="action" value="Search" /></td>
				</tr>
			</table>
		</form:form>
	</sec:authorize>

	<br>
	<table border="1">
		
		<th>RFC</th>
		<th>CURP</th>
		<th>Nombre</th>
		<th>Apellido Paterno</th>
		<th>Apellido Materno</th>
		<th>Raz�n social</th>
		<th>Correo electr�nico</th>
		<th>Telefono</th>
		<c:forEach items="${receptorList}" var="receptor">
			<tr>				
				<td>${receptor.rrfc}</td>
				<td>${receptor.rcurp}</td>
				<td>${receptor.rnombre}</td>
				<td>${receptor.rapellpat}</td>
				<td>${receptor.rapellmat}</td>
				<td>${receptor.rdenrazsoc}</td>
				<td>${receptor.rcorreoe}</td>
				<td>${receptor.rnumtel}</td>
			</tr>

		</c:forEach>
	</table>


</body>
</html>