<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DATOS DE IDENTIFICACIÓN</title>
	<style type="text/css">
		body {
			font-family: verdana, sans-serif;			
		}
		
		span.campoConError {
			color: red;
		}
		
	</style>	
</head>
<body>
	<c:url value="/datos" var="destino"/>		
	<form:form method="post" action="${destino}" commandName="datosIdentificacion">
		<h1>DATOS DE IDENTIFICACIÓN</h1>
		<table>
			<tbody>
				<tr>
					<td>Registro Federal de Contribuyentes:</td>
					<td><form:input path="rfc" /></td>
					<td><form:errors path="rfc" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td>Curp:</td>
					<td><form:input path="curp" /></td>
					<td><form:errors path="curp" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td>Ejercicio:</td>
					<td><form:input path="ejercicio" maxlength="4" /></td>
					<td><form:errors path="ejercicio" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td>Apellido Paterno:</td>
					<td><form:input path="aPaterno" maxlength="6" /></td>
					<td><form:errors path="aPaterno" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td>Apellido Materno:</td>
					<td><form:input path="aMaterno" maxlength="6" /></td>
					<td><form:errors path="aMaterno" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td>Nombre:</td>
					<td><form:input path="nombre" maxlength="6" /></td>
					<td><form:errors path="nombre" cssClass="campoConError" /></td>
				</tr>
				<tr>
					<td colspan="3"><input type="submit" value="Enviar" /></td>
				</tr>
			</tbody>
		</table>
	</form:form>






</body>
</html>