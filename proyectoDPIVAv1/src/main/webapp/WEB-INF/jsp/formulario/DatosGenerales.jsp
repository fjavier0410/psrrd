<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DATOS GENERALES</title>
</head>
<body>
	<c:url value="/datos" var="destino"/>	
	<form:form method="POST" action="${destino}" commandName="datos">

		<table>
			

		<!-- 	<tr>
				<td>Declaración Informativa de Operaciones con Terceros</td>
				<td><form:select path="operacionesTerceros" onchange="">
						<form:option value="NONE" label="--- Select ---" />
						<form:options items="${operacionesTercerosList}" />
					</form:select></td>
			</tr> -->
			
			<tr>
				<td>Tipo de Declaración</td>
				<td><form:select path="tipoDeclaracion">
						<form:option value="NONE" label="--- Select ---" />
						<form:options items="${tipoDeclaracionList}" />
					</form:select></td>
			</tr>
			
			
			<tr>
				<td>Número de Operación o Folio Anterior</td>
				<td><form:input path="folioAnterior"/></td>
			</tr>
			
			<tr>
				<td>Fecha de presentación Anterior</td>
				<td><form:input path="fechaPresentacionAnterior"/>
			</tr>

			<tr>
				<td>Tipo de Periodicidad</td>
				<td><form:select path="tipoPeriodicidad" onchange="AldatuC('bat','bi')" name="bat" id="bat">
						<form:option value="NONE" label="--- Select ---" />
						<form:options items="${tipoPeriodicidadList}" />
					</form:select></td>
			</tr>
			
			<tr>
				<td>Periodo</td>
				<td><form:select path="periodoMensual" name="bi" id="bi">
						<form:option value="NONE" label="--- Select ---" />
					</form:select></td>
			</tr>
			
			

			<tr>
				<td colspan="3"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
<script type="text/javascript">
 function KargaC()
  {
    var s1=document.getElementById('bat');
    var s2=document.getElementById('bi');
    s1.options[0]=new Option("Bilbo","0","0");
    s1.options[1]=new Option("Bermio","1","1");        
    s1.selectedIndex=0;
    s2.options[0]=new Option("Moyua","0","0");    
    s2.options[1]=new Option("Abando","1","1");
    s2.selectedIndex=0;
  }
 function AldatuC(uan,txu)
  {  
   var s1=document.getElementById(uan);
   var s2=document.getElementById(txu);
   
   alert(s1)
   alert(s2)
   s2.options.length = 0;
   if(s1.selectedIndex != 0){
   if(s1.selectedIndex==1)
    {    	 
	 s2.options[0]=new Option("Enero","0","0");
	 s2.options[1]=new Option("Febrero","1","1");
	 s2.options[2]=new Option("Marzo","2","2");
	 s2.options[3]=new Option("Abril","3","3");	
	 s2.options[4]=new Option("Mayo","4","4");
	 s2.options[5]=new Option("Junio","5","5");	
	 s2.options[6]=new Option("Julio","6","6");
	 s2.options[7]=new Option("Agosto","7","7");	
	 s2.options[8]=new Option("Septiembre","8","8");
	 s2.options[9]=new Option("Octubre","9","9");
	 s2.options[10]=new Option("Noviembre","10","10");
	 s2.options[11]=new Option("Diciembre","11","11");
	 s2.selectedIndex=0;
	}
   if(s1.selectedIndex==2)
    {     
	 s2.options[0]=new Option("Enero - Febrero","0","0");	 
	 s2.options[1]=new Option("Marzo - Abril","1","1");	
	 s2.options[0]=new Option("Mayo - Junio","2","2");	 
	 s2.options[1]=new Option("Julio - Agosto","3","3");	
	 s2.options[0]=new Option("Septimbre - Octubre","4","4");	 
	 s2.options[1]=new Option("Noviembre - Diciembre","5","5");	
	 s2.selectedIndex=0;
	} 
   if(s1.selectedIndex==3)
   {     
	   s2.options[0]=new Option("Enero - Marzo","0","0");	 
	   s2.options[1]=new Option("Abril - Junio","1","1");	
	   s2.options[0]=new Option("Julio - Septiembre","2","2");	 
	   s2.options[1]=new Option("Octubre - Diciembre","3","3");	
	 s2.selectedIndex=0;
	}
   if(s1.selectedIndex==4)
   {     
	 s2.options[0]=new Option("Enero - Abril","0","0");	 
	 s2.options[1]=new Option("Mayo - Agosto","1","1");
	 s2.options[1]=new Option("Semptiembre - Diciembre","2","2");
	 s2.selectedIndex=0;
	}
   if(s1.selectedIndex==5)
   {     
	 s2.options[0]=new Option("Enero - Junio","0","0");	 
	 s2.options[1]=new Option("Julio - Diciembre","1","1");
	 s2.selectedIndex=0;
	} 
   }
  }
</script>


</html>