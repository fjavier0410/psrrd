package com.quadrum.dpiva.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quadrum.dpiva.dao.TotalesDao;
import com.quadrum.dpiva.model.Totales;
import com.quadrum.dpiva.model.Usuario;

@Repository
public class TotalesDaoImpl implements TotalesDao {
	@Autowired
	private SessionFactory session;
	
	

	@Override
	public void addTotales(Totales totales) {
		session.getCurrentSession().save(totales);

	}

	@Override
	public void editTotales(Totales totales) {
		session.getCurrentSession().update(totales);
	}

	@Override
	public void deleteTotales(int idTotales) {
		session.getCurrentSession().delete(findTotales(idTotales));

	}

	@Override
	public Totales findTotales(int idTotales) {
		return (Totales)session.getCurrentSession().get(Usuario.class,idTotales);
	}

	@Override
	public List<Totales> getAllTotales() {
		return session.getCurrentSession().createQuery("from Totales").list();
	}

}
