package com.quadrum.dpiva.dao;

import java.util.List;

import com.quadrum.dpiva.model.Receptor;

public interface ReceptorDao {

	public void add(Receptor receptor);

	public void edit(Receptor receptor);

	public void delete(int idReceptor);

	public Receptor getReceptor(int idReceptor);

	public List getAllReceptor();
	
	public Receptor getReceptorRfc(String rrfc);

}
