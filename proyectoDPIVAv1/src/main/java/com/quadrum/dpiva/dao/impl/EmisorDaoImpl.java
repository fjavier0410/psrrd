package com.quadrum.dpiva.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quadrum.dpiva.dao.EmisorDao;
import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.Receptor;

@Repository
public class EmisorDaoImpl implements EmisorDao {
	@Autowired
	private SessionFactory session;

	@Override
	public void add(Emisor emisor) {
		session.getCurrentSession().save(emisor);
	}

	@Override
	public void edit(Emisor emisor) {
		session.getCurrentSession().update(emisor);
	}

	@Override
	public void delete(int idEmisor) {
		session.getCurrentSession().delete(getEmisor(idEmisor));
	}

	@Override
	public Emisor getEmisor(int idEmisor) {
		
		return (Emisor)session.getCurrentSession().get(Emisor.class, idEmisor);
	}

	@Override
	public List getAllEmisor() {
		return session.getCurrentSession().createQuery("from Emisor").list();
	}

	@Override
	public Emisor buscaPorRFc(String rrfc) {
		Query query = session.getCurrentSession().createQuery("from Emisor where erfc = :erfc");
		query.setParameter("erfc",rrfc);
		Emisor emisor = (Emisor)query.uniqueResult();		
		return emisor;
	}

}
