package com.quadrum.dpiva.dao;

import java.util.List;

import com.quadrum.dpiva.model.Usuario;

public interface UsuarioDao {

	public void addUsuario(Usuario usuario);

	public void editUsuario(Usuario usuario);

	public void deleteUsuario(int idUsuario);

	public Usuario findUsuario(int idUsuario);
	
	public Usuario findNombreUsuario(String nombre);

	public List<Usuario> getAllUsuario();

}
