package com.quadrum.dpiva.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quadrum.dpiva.dao.ReceptorDao;
import com.quadrum.dpiva.model.Receptor;


@Repository
public class ReceptorDaoImpl implements ReceptorDao {
	@Autowired
	private SessionFactory session;

	@Override
	public void add(Receptor receptor) {
		session.getCurrentSession().save(receptor);
	}

	@Override
	public void edit(Receptor receptor) {
		System.out.println("OBJ "+receptor.getIdRECEPTOR());
		session.getCurrentSession().update(receptor);
	}

	@Override
	public void delete(int idReceptor) {
		session.getCurrentSession().delete(getReceptor(idReceptor));
	}

	@Override
	public Receptor getReceptor(int idReceptor) {
		return (Receptor)session.getCurrentSession().get(Receptor.class, idReceptor);
	}

	@Override
	public List getAllReceptor() {
		return session.getCurrentSession().createQuery("from Receptor").list();
	}

	@Override
	public Receptor getReceptorRfc(String rrfc) {
		Query query = session.getCurrentSession().createQuery("from Receptor where rrfc = :rrfc");
		query.setParameter("rrfc",rrfc);
		Receptor receptor = (Receptor)query.uniqueResult();		
		return receptor;
	}

}
