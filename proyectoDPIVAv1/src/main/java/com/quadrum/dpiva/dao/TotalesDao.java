package com.quadrum.dpiva.dao;

import java.util.List;

import com.quadrum.dpiva.model.Totales;

public interface TotalesDao {
	
	public void addTotales(Totales totales);
	
	public void editTotales(Totales totales);
	
	public void deleteTotales(int idTotales);
	
	public Totales findTotales(int idTotales); 
	
	public List<Totales> getAllTotales(); 
	

}
