package com.quadrum.dpiva.dao;

import java.util.List;

import com.quadrum.dpiva.model.Emisor;

public interface EmisorDao {
	
	public void add(Emisor emisor);
	
	public void edit(Emisor emisor);
	
	public void delete(int idEmisor);
	
	public Emisor getEmisor(int idEmisor);
	
	public List getAllEmisor();
	
	public Emisor buscaPorRFc(String rfc); 

}
