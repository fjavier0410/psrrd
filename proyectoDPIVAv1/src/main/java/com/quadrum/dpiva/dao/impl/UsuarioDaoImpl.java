package com.quadrum.dpiva.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quadrum.dpiva.dao.UsuarioDao;
import com.quadrum.dpiva.model.Usuario;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {
	@Autowired
	private SessionFactory session;
	

	@Override
	public void addUsuario(Usuario usuario) {
		session.getCurrentSession().save(usuario);
	}

	@Override
	public void editUsuario(Usuario usuario) {
		session.getCurrentSession().update(usuario);
	}

	@Override
	public void deleteUsuario(int idUsuario) {
		session.getCurrentSession().delete(findUsuario(idUsuario));
	}

	@Override
	public Usuario findUsuario(int idUsuario) {
		return (Usuario) session.getCurrentSession().createQuery("from Usuario where idusuario = :idUsuario").setParameter("idUsuario", idUsuario).uniqueResult();
	}

	@Override
	public Usuario findNombreUsuario(String nombre) {
		Criteria criteria = session.getCurrentSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("nomUsuario", nombre));
		return (Usuario) criteria.uniqueResult();
	}

	@Override
	public List<Usuario> getAllUsuario() {
		return session.getCurrentSession().createQuery("from Usuario").list();
	}

}
