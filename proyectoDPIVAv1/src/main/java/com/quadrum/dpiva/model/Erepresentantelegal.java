/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "erepresentantelegal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Erepresentantelegal.findAll", query = "SELECT e FROM Erepresentantelegal e"),
    @NamedQuery(name = "Erepresentantelegal.findByIdEREPRESENTANTELEGAL", query = "SELECT e FROM Erepresentantelegal e WHERE e.idEREPRESENTANTELEGAL = :idEREPRESENTANTELEGAL"),
    @NamedQuery(name = "Erepresentantelegal.findByErfcrep", query = "SELECT e FROM Erepresentantelegal e WHERE e.erfcrep = :erfcrep"),
    @NamedQuery(name = "Erepresentantelegal.findByEcurprep", query = "SELECT e FROM Erepresentantelegal e WHERE e.ecurprep = :ecurprep"),
    @NamedQuery(name = "Erepresentantelegal.findByEapellpatrep", query = "SELECT e FROM Erepresentantelegal e WHERE e.eapellpatrep = :eapellpatrep"),
    @NamedQuery(name = "Erepresentantelegal.findByEapellmatrep", query = "SELECT e FROM Erepresentantelegal e WHERE e.eapellmatrep = :eapellmatrep"),
    @NamedQuery(name = "Erepresentantelegal.findByEnombrerep", query = "SELECT e FROM Erepresentantelegal e WHERE e.enombrerep = :enombrerep")})
public class Erepresentantelegal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEREPRESENTANTELEGAL")
    private Integer idEREPRESENTANTELEGAL;
    @Column(name = "erfcrep")
    private String erfcrep;
    @Column(name = "ecurprep")
    private String ecurprep;
    @Column(name = "eapellpatrep")
    private String eapellpatrep;
    @Column(name = "eapellmatrep")
    private String eapellmatrep;
    @Column(name = "enombrerep")
    private String enombrerep;

    public Erepresentantelegal() {
    }

    public Erepresentantelegal(Integer idEREPRESENTANTELEGAL) {
        this.idEREPRESENTANTELEGAL = idEREPRESENTANTELEGAL;
    }

    public Integer getIdEREPRESENTANTELEGAL() {
        return idEREPRESENTANTELEGAL;
    }

    public void setIdEREPRESENTANTELEGAL(Integer idEREPRESENTANTELEGAL) {
        this.idEREPRESENTANTELEGAL = idEREPRESENTANTELEGAL;
    }

    public String getErfcrep() {
        return erfcrep;
    }

    public void setErfcrep(String erfcrep) {
        this.erfcrep = erfcrep;
    }

    public String getEcurprep() {
        return ecurprep;
    }

    public void setEcurprep(String ecurprep) {
        this.ecurprep = ecurprep;
    }

    public String getEapellpatrep() {
        return eapellpatrep;
    }

    public void setEapellpatrep(String eapellpatrep) {
        this.eapellpatrep = eapellpatrep;
    }

    public String getEapellmatrep() {
        return eapellmatrep;
    }

    public void setEapellmatrep(String eapellmatrep) {
        this.eapellmatrep = eapellmatrep;
    }

    public String getEnombrerep() {
        return enombrerep;
    }

    public void setEnombrerep(String enombrerep) {
        this.enombrerep = enombrerep;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEREPRESENTANTELEGAL != null ? idEREPRESENTANTELEGAL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Erepresentantelegal)) {
            return false;
        }
        Erepresentantelegal other = (Erepresentantelegal) object;
        if ((this.idEREPRESENTANTELEGAL == null && other.idEREPRESENTANTELEGAL != null) || (this.idEREPRESENTANTELEGAL != null && !this.idEREPRESENTANTELEGAL.equals(other.idEREPRESENTANTELEGAL))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Erepresentantelegal[ idEREPRESENTANTELEGAL=" + idEREPRESENTANTELEGAL + " ]";
    }
    
}
