/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "eunidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eunidad.findAll", query = "SELECT e FROM Eunidad e"),
    @NamedQuery(name = "Eunidad.findByIdEUNIDAD", query = "SELECT e FROM Eunidad e WHERE e.idEUNIDAD = :idEUNIDAD"),
    @NamedQuery(name = "Eunidad.findByEunidad", query = "SELECT e FROM Eunidad e WHERE e.eunidad = :eunidad"),
    @NamedQuery(name = "Eunidad.findByEEMPLEADODEidEEMPLEADODE", query = "SELECT e FROM Eunidad e WHERE e.eEMPLEADODEidEEMPLEADODE = :eEMPLEADODEidEEMPLEADODE")})
public class Eunidad implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEUNIDAD")
    private Integer idEUNIDAD;
    @Column(name = "eunidad")
    private String eunidad;
    @Basic(optional = false)
    @Column(name = "EEMPLEADODE_idEEMPLEADODE")
    private int eEMPLEADODEidEEMPLEADODE;

    public Eunidad() {
    }

    public Eunidad(Integer idEUNIDAD) {
        this.idEUNIDAD = idEUNIDAD;
    }

    public Eunidad(Integer idEUNIDAD, int eEMPLEADODEidEEMPLEADODE) {
        this.idEUNIDAD = idEUNIDAD;
        this.eEMPLEADODEidEEMPLEADODE = eEMPLEADODEidEEMPLEADODE;
    }

    public Integer getIdEUNIDAD() {
        return idEUNIDAD;
    }

    public void setIdEUNIDAD(Integer idEUNIDAD) {
        this.idEUNIDAD = idEUNIDAD;
    }

    public String getEunidad() {
        return eunidad;
    }

    public void setEunidad(String eunidad) {
        this.eunidad = eunidad;
    }

    public int getEEMPLEADODEidEEMPLEADODE() {
        return eEMPLEADODEidEEMPLEADODE;
    }

    public void setEEMPLEADODEidEEMPLEADODE(int eEMPLEADODEidEEMPLEADODE) {
        this.eEMPLEADODEidEEMPLEADODE = eEMPLEADODEidEEMPLEADODE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEUNIDAD != null ? idEUNIDAD.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eunidad)) {
            return false;
        }
        Eunidad other = (Eunidad) object;
        if ((this.idEUNIDAD == null && other.idEUNIDAD != null) || (this.idEUNIDAD != null && !this.idEUNIDAD.equals(other.idEUNIDAD))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Eunidad[ idEUNIDAD=" + idEUNIDAD + " ]";
    }
    
}
