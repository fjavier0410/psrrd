/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "eempleadode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eempleadode.findAll", query = "SELECT e FROM Eempleadode e"),
    @NamedQuery(name = "Eempleadode.findByIdEEMPLEADODE", query = "SELECT e FROM Eempleadode e WHERE e.idEEMPLEADODE = :idEEMPLEADODE"),
    @NamedQuery(name = "Eempleadode.findByErfcorg", query = "SELECT e FROM Eempleadode e WHERE e.erfcorg = :erfcorg"),
    @NamedQuery(name = "Eempleadode.findByEdenorazsocorg", query = "SELECT e FROM Eempleadode e WHERE e.edenorazsocorg = :edenorazsocorg")})
public class Eempleadode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEEMPLEADODE")
    private Integer idEEMPLEADODE;
    @Column(name = "erfcorg")
    private String erfcorg;
    @Column(name = "edenorazsocorg")
    private String edenorazsocorg;

    public Eempleadode() {
    }

    public Eempleadode(Integer idEEMPLEADODE) {
        this.idEEMPLEADODE = idEEMPLEADODE;
    }

    public Integer getIdEEMPLEADODE() {
        return idEEMPLEADODE;
    }

    public void setIdEEMPLEADODE(Integer idEEMPLEADODE) {
        this.idEEMPLEADODE = idEEMPLEADODE;
    }

    public String getErfcorg() {
        return erfcorg;
    }

    public void setErfcorg(String erfcorg) {
        this.erfcorg = erfcorg;
    }

    public String getEdenorazsocorg() {
        return edenorazsocorg;
    }

    public void setEdenorazsocorg(String edenorazsocorg) {
        this.edenorazsocorg = edenorazsocorg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEEMPLEADODE != null ? idEEMPLEADODE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eempleadode)) {
            return false;
        }
        Eempleadode other = (Eempleadode) object;
        if ((this.idEEMPLEADODE == null && other.idEEMPLEADODE != null) || (this.idEEMPLEADODE != null && !this.idEEMPLEADODE.equals(other.idEEMPLEADODE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Eempleadode[ idEEMPLEADODE=" + idEEMPLEADODE + " ]";
    }
    
}
