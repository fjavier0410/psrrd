/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "documentodigital")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documentodigital.findAll", query = "SELECT d FROM Documentodigital d"),
    @NamedQuery(name = "Documentodigital.findByIdDOCUMENTODIGITAL", query = "SELECT d FROM Documentodigital d WHERE d.idDOCUMENTODIGITAL = :idDOCUMENTODIGITAL"),
    @NamedQuery(name = "Documentodigital.findByVersion", query = "SELECT d FROM Documentodigital d WHERE d.version = :version"),
    @NamedQuery(name = "Documentodigital.findByTipodedocumentodigital", query = "SELECT d FROM Documentodigital d WHERE d.tipodedocumentodigital = :tipodedocumentodigital"),
    @NamedQuery(name = "Documentodigital.findByCert", query = "SELECT d FROM Documentodigital d WHERE d.cert = :cert"),
    @NamedQuery(name = "Documentodigital.findByNumcert", query = "SELECT d FROM Documentodigital d WHERE d.numcert = :numcert"),
    @NamedQuery(name = "Documentodigital.findByFirma", query = "SELECT d FROM Documentodigital d WHERE d.firma = :firma"),
    @NamedQuery(name = "Documentodigital.findByEMISORidEMISOR", query = "SELECT d FROM Documentodigital d WHERE d.eMISORidEMISOR = :eMISORidEMISOR"),
    @NamedQuery(name = "Documentodigital.findByRECEPTORidRECEPTOR", query = "SELECT d FROM Documentodigital d WHERE d.rECEPTORidRECEPTOR = :rECEPTORidRECEPTOR"),
    @NamedQuery(name = "Documentodigital.findByDPIVAidDPIVA", query = "SELECT d FROM Documentodigital d WHERE d.dPIVAidDPIVA = :dPIVAidDPIVA")})
public class Documentodigital implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDOCUMENTODIGITAL")
    private Integer idDOCUMENTODIGITAL;
    @Column(name = "version")
    private String version;
    @Column(name = "tipodedocumentodigital")
    private String tipodedocumentodigital;
    @Column(name = "cert")
    private String cert;
    @Column(name = "numcert")
    private String numcert;
    @Column(name = "firma")
    private String firma;
    @Basic(optional = false)
    @Column(name = "EMISOR_idEMISOR")
    private int eMISORidEMISOR;
    @Basic(optional = false)
    @Column(name = "RECEPTOR_idRECEPTOR")
    private int rECEPTORidRECEPTOR;
    @Basic(optional = false)
    @Column(name = "DPIVA_idDPIVA")
    private int dPIVAidDPIVA;
    
    private String operacionesTerceros;

    public String getOperacionesTerceros() {
		return operacionesTerceros;
	}

	public void setOperacionesTerceros(String operacionesTerceros) {
		this.operacionesTerceros = operacionesTerceros;
	}

	public Documentodigital() {
    }

    public Documentodigital(Integer idDOCUMENTODIGITAL) {
        this.idDOCUMENTODIGITAL = idDOCUMENTODIGITAL;
    }

    public Documentodigital(Integer idDOCUMENTODIGITAL, int eMISORidEMISOR, int rECEPTORidRECEPTOR, int dPIVAidDPIVA) {
        this.idDOCUMENTODIGITAL = idDOCUMENTODIGITAL;
        this.eMISORidEMISOR = eMISORidEMISOR;
        this.rECEPTORidRECEPTOR = rECEPTORidRECEPTOR;
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    public Integer getIdDOCUMENTODIGITAL() {
        return idDOCUMENTODIGITAL;
    }

    public void setIdDOCUMENTODIGITAL(Integer idDOCUMENTODIGITAL) {
        this.idDOCUMENTODIGITAL = idDOCUMENTODIGITAL;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTipodedocumentodigital() {
        return tipodedocumentodigital;
    }

    public void setTipodedocumentodigital(String tipodedocumentodigital) {
        this.tipodedocumentodigital = tipodedocumentodigital;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getNumcert() {
        return numcert;
    }

    public void setNumcert(String numcert) {
        this.numcert = numcert;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public int getEMISORidEMISOR() {
        return eMISORidEMISOR;
    }

    public void setEMISORidEMISOR(int eMISORidEMISOR) {
        this.eMISORidEMISOR = eMISORidEMISOR;
    }

    public int getRECEPTORidRECEPTOR() {
        return rECEPTORidRECEPTOR;
    }

    public void setRECEPTORidRECEPTOR(int rECEPTORidRECEPTOR) {
        this.rECEPTORidRECEPTOR = rECEPTORidRECEPTOR;
    }

    public int getDPIVAidDPIVA() {
        return dPIVAidDPIVA;
    }

    public void setDPIVAidDPIVA(int dPIVAidDPIVA) {
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDOCUMENTODIGITAL != null ? idDOCUMENTODIGITAL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documentodigital)) {
            return false;
        }
        Documentodigital other = (Documentodigital) object;
        if ((this.idDOCUMENTODIGITAL == null && other.idDOCUMENTODIGITAL != null) || (this.idDOCUMENTODIGITAL != null && !this.idDOCUMENTODIGITAL.equals(other.idDOCUMENTODIGITAL))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Documentodigital[ idDOCUMENTODIGITAL=" + idDOCUMENTODIGITAL + " ]";
    }
    
}
