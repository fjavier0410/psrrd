/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "receptor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Receptor.findAll", query = "SELECT r FROM Receptor r"),
    @NamedQuery(name = "Receptor.findByIdRECEPTOR", query = "SELECT r FROM Receptor r WHERE r.idRECEPTOR = :idRECEPTOR"),
    @NamedQuery(name = "Receptor.findByRrfc", query = "SELECT r FROM Receptor r WHERE r.rrfc = :rrfc"),
    @NamedQuery(name = "Receptor.findByRcurp", query = "SELECT r FROM Receptor r WHERE r.rcurp = :rcurp"),
    @NamedQuery(name = "Receptor.findByRapellpat", query = "SELECT r FROM Receptor r WHERE r.rapellpat = :rapellpat"),
    @NamedQuery(name = "Receptor.findByRapellmat", query = "SELECT r FROM Receptor r WHERE r.rapellmat = :rapellmat"),
    @NamedQuery(name = "Receptor.findByRnombre", query = "SELECT r FROM Receptor r WHERE r.rnombre = :rnombre"),
    @NamedQuery(name = "Receptor.findByRdenrazsoc", query = "SELECT r FROM Receptor r WHERE r.rdenrazsoc = :rdenrazsoc"),
    @NamedQuery(name = "Receptor.findByRcorreoe", query = "SELECT r FROM Receptor r WHERE r.rcorreoe = :rcorreoe"),
    @NamedQuery(name = "Receptor.findByRnumtel", query = "SELECT r FROM Receptor r WHERE r.rnumtel = :rnumtel")})
public class Receptor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRECEPTOR")
    private Integer idRECEPTOR;
    @Column(name = "rrfc")
    private String rrfc;
    @Column(name = "rcurp")
    private String rcurp;
    @Column(name = "rapellpat")
    private String rapellpat;
    @Column(name = "rapellmat")
    private String rapellmat;
    @Column(name = "rnombre")
    private String rnombre;
    @Column(name = "rdenrazsoc")
    private String rdenrazsoc;
    @Column(name = "rcorreoe")
    private String rcorreoe;
    @Column(name = "rnumtel")
    private String rnumtel;
    

    public Receptor() {
    }

    public Receptor(Integer idRECEPTOR) {
        this.idRECEPTOR = idRECEPTOR;
    }

    public Integer getIdRECEPTOR() {
        return idRECEPTOR;
    }

    public void setIdRECEPTOR(Integer idRECEPTOR) {
        this.idRECEPTOR = idRECEPTOR;
    }

    public String getRrfc() {
        return rrfc;
    }

    public void setRrfc(String rrfc) {
        this.rrfc = rrfc;
    }

    public String getRcurp() {
        return rcurp;
    }

    public void setRcurp(String rcurp) {
        this.rcurp = rcurp;
    }

    public String getRapellpat() {
        return rapellpat;
    }

    public void setRapellpat(String rapellpat) {
        this.rapellpat = rapellpat;
    }

    public String getRapellmat() {
        return rapellmat;
    }

    public void setRapellmat(String rapellmat) {
        this.rapellmat = rapellmat;
    }

    public String getRnombre() {
        return rnombre;
    }

    public void setRnombre(String rnombre) {
        this.rnombre = rnombre;
    }

    public String getRdenrazsoc() {
        return rdenrazsoc;
    }

    public void setRdenrazsoc(String rdenrazsoc) {
        this.rdenrazsoc = rdenrazsoc;
    }

    public String getRcorreoe() {
        return rcorreoe;
    }

    public void setRcorreoe(String rcorreoe) {
        this.rcorreoe = rcorreoe;
    }

    public String getRnumtel() {
        return rnumtel;
    }

    public void setRnumtel(String rnumtel) {
        this.rnumtel = rnumtel;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRECEPTOR != null ? idRECEPTOR.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Receptor)) {
            return false;
        }
        Receptor other = (Receptor) object;
        if ((this.idRECEPTOR == null && other.idRECEPTOR != null) || (this.idRECEPTOR != null && !this.idRECEPTOR.equals(other.idRECEPTOR))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Receptor[ idRECEPTOR=" + idRECEPTOR + " ]";
    }
    
}
