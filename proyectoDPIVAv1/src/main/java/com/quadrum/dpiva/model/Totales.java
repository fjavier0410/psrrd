/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "totales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Totales.findAll", query = "SELECT t FROM Totales t"),
    @NamedQuery(name = "Totales.findByIdTOTALES", query = "SELECT t FROM Totales t WHERE t.idTOTALES = :idTOTALES"),
    @NamedQuery(name = "Totales.findByTotoperac", query = "SELECT t FROM Totales t WHERE t.totoperac = :totoperac"),
    @NamedQuery(name = "Totales.findByTotactpagtas15o16IVA", query = "SELECT t FROM Totales t WHERE t.totactpagtas15o16IVA = :totactpagtas15o16IVA"),
    @NamedQuery(name = "Totales.findByTotactpagtas15IVA", query = "SELECT t FROM Totales t WHERE t.totactpagtas15IVA = :totactpagtas15IVA"),
    @NamedQuery(name = "Totales.findByTotIVApagnoacrtas15o16", query = "SELECT t FROM Totales t WHERE t.totIVApagnoacrtas15o16 = :totIVApagnoacrtas15o16"),
    @NamedQuery(name = "Totales.findByTotactpagtas10u11IVA", query = "SELECT t FROM Totales t WHERE t.totactpagtas10u11IVA = :totactpagtas10u11IVA"),
    @NamedQuery(name = "Totales.findByTotactpagtas10IVA", query = "SELECT t FROM Totales t WHERE t.totactpagtas10IVA = :totactpagtas10IVA"),
    @NamedQuery(name = "Totales.findByTotIVApagnoacrtas10u11", query = "SELECT t FROM Totales t WHERE t.totIVApagnoacrtas10u11 = :totIVApagnoacrtas10u11"),
    @NamedQuery(name = "Totales.findByTotactpagimpbystas15o16IVA", query = "SELECT t FROM Totales t WHERE t.totactpagimpbystas15o16IVA = :totactpagimpbystas15o16IVA"),
    @NamedQuery(name = "Totales.findByTotIVApagnoacrimptas15o16", query = "SELECT t FROM Totales t WHERE t.totIVApagnoacrimptas15o16 = :totIVApagnoacrimptas15o16"),
    @NamedQuery(name = "Totales.findByTotactpagimpbystas10u11IVA", query = "SELECT t FROM Totales t WHERE t.totactpagimpbystas10u11IVA = :totactpagimpbystas10u11IVA"),
    @NamedQuery(name = "Totales.findByTotIVApagnoacrimptas10u11", query = "SELECT t FROM Totales t WHERE t.totIVApagnoacrimptas10u11 = :totIVApagnoacrimptas10u11"),
    @NamedQuery(name = "Totales.findByTotactpagimpbysnopagIVA", query = "SELECT t FROM Totales t WHERE t.totactpagimpbysnopagIVA = :totactpagimpbysnopagIVA"),
    @NamedQuery(name = "Totales.findByTotdemactpagtas0IVA", query = "SELECT t FROM Totales t WHERE t.totdemactpagtas0IVA = :totdemactpagtas0IVA"),
    @NamedQuery(name = "Totales.findByTotactpagnopagIVA", query = "SELECT t FROM Totales t WHERE t.totactpagnopagIVA = :totactpagnopagIVA"),
    @NamedQuery(name = "Totales.findByTotIVAretcont", query = "SELECT t FROM Totales t WHERE t.totIVAretcont = :totIVAretcont"),
    @NamedQuery(name = "Totales.findByTotIVAdevdescybonifcomp", query = "SELECT t FROM Totales t WHERE t.totIVAdevdescybonifcomp = :totIVAdevdescybonifcomp"),
    @NamedQuery(name = "Totales.findByTotIVAtraslcontexcimpByS", query = "SELECT t FROM Totales t WHERE t.totIVAtraslcontexcimpByS = :totIVAtraslcontexcimpByS"),
    @NamedQuery(name = "Totales.findByTotIVApagimpByS", query = "SELECT t FROM Totales t WHERE t.totIVApagimpByS = :totIVApagimpByS"),
    @NamedQuery(name = "Totales.findByDPIVAidDPIVA", query = "SELECT t FROM Totales t WHERE t.dPIVAidDPIVA = :dPIVAidDPIVA")})
public class Totales implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTOTALES")
    private Integer idTOTALES;
    @Column(name = "totoperac")
    private Integer totoperac;
    @Column(name = "totactpagtas15o16IVA")
    private Integer totactpagtas15o16IVA;
    @Column(name = "totactpagtas15IVA")
    private Integer totactpagtas15IVA;
    @Column(name = "totIVApagnoacrtas15o16")
    private Integer totIVApagnoacrtas15o16;
    @Column(name = "totactpagtas10u11IVA")
    private Integer totactpagtas10u11IVA;
    @Column(name = "totactpagtas10IVA")
    private Integer totactpagtas10IVA;
    @Column(name = "totIVApagnoacrtas10u11")
    private Integer totIVApagnoacrtas10u11;
    @Column(name = "totactpagimpbystas15o16IVA")
    private Integer totactpagimpbystas15o16IVA;
    @Column(name = "totIVApagnoacrimptas15o16")
    private Integer totIVApagnoacrimptas15o16;
    @Column(name = "totactpagimpbystas10u11IVA")
    private Integer totactpagimpbystas10u11IVA;
    @Column(name = "totIVApagnoacrimptas10u11")
    private Integer totIVApagnoacrimptas10u11;
    @Column(name = "totactpagimpbysnopagIVA")
    private Integer totactpagimpbysnopagIVA;
    @Column(name = "totdemactpagtas0IVA")
    private Integer totdemactpagtas0IVA;
    @Column(name = "totactpagnopagIVA")
    private Integer totactpagnopagIVA;
    @Column(name = "totIVAretcont")
    private Integer totIVAretcont;
    @Column(name = "totIVAdevdescybonifcomp")
    private Integer totIVAdevdescybonifcomp;
    @Column(name = "totIVAtraslcontexcimpByS")
    private Integer totIVAtraslcontexcimpByS;
    @Column(name = "totIVApagimpByS")
    private Integer totIVApagimpByS;
    @Basic(optional = false)
    @Column(name = "DPIVA_idDPIVA")
    private int dPIVAidDPIVA;

    public Totales() {
    }

    public Totales(Integer idTOTALES) {
        this.idTOTALES = idTOTALES;
    }

    public Totales(Integer idTOTALES, int dPIVAidDPIVA) {
        this.idTOTALES = idTOTALES;
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    public Integer getIdTOTALES() {
        return idTOTALES;
    }

    public void setIdTOTALES(Integer idTOTALES) {
        this.idTOTALES = idTOTALES;
    }

    public Integer getTotoperac() {
        return totoperac;
    }

    public void setTotoperac(Integer totoperac) {
        this.totoperac = totoperac;
    }

    public Integer getTotactpagtas15o16IVA() {
        return totactpagtas15o16IVA;
    }

    public void setTotactpagtas15o16IVA(Integer totactpagtas15o16IVA) {
        this.totactpagtas15o16IVA = totactpagtas15o16IVA;
    }

    public Integer getTotactpagtas15IVA() {
        return totactpagtas15IVA;
    }

    public void setTotactpagtas15IVA(Integer totactpagtas15IVA) {
        this.totactpagtas15IVA = totactpagtas15IVA;
    }

    public Integer getTotIVApagnoacrtas15o16() {
        return totIVApagnoacrtas15o16;
    }

    public void setTotIVApagnoacrtas15o16(Integer totIVApagnoacrtas15o16) {
        this.totIVApagnoacrtas15o16 = totIVApagnoacrtas15o16;
    }

    public Integer getTotactpagtas10u11IVA() {
        return totactpagtas10u11IVA;
    }

    public void setTotactpagtas10u11IVA(Integer totactpagtas10u11IVA) {
        this.totactpagtas10u11IVA = totactpagtas10u11IVA;
    }

    public Integer getTotactpagtas10IVA() {
        return totactpagtas10IVA;
    }

    public void setTotactpagtas10IVA(Integer totactpagtas10IVA) {
        this.totactpagtas10IVA = totactpagtas10IVA;
    }

    public Integer getTotIVApagnoacrtas10u11() {
        return totIVApagnoacrtas10u11;
    }

    public void setTotIVApagnoacrtas10u11(Integer totIVApagnoacrtas10u11) {
        this.totIVApagnoacrtas10u11 = totIVApagnoacrtas10u11;
    }

    public Integer getTotactpagimpbystas15o16IVA() {
        return totactpagimpbystas15o16IVA;
    }

    public void setTotactpagimpbystas15o16IVA(Integer totactpagimpbystas15o16IVA) {
        this.totactpagimpbystas15o16IVA = totactpagimpbystas15o16IVA;
    }

    public Integer getTotIVApagnoacrimptas15o16() {
        return totIVApagnoacrimptas15o16;
    }

    public void setTotIVApagnoacrimptas15o16(Integer totIVApagnoacrimptas15o16) {
        this.totIVApagnoacrimptas15o16 = totIVApagnoacrimptas15o16;
    }

    public Integer getTotactpagimpbystas10u11IVA() {
        return totactpagimpbystas10u11IVA;
    }

    public void setTotactpagimpbystas10u11IVA(Integer totactpagimpbystas10u11IVA) {
        this.totactpagimpbystas10u11IVA = totactpagimpbystas10u11IVA;
    }

    public Integer getTotIVApagnoacrimptas10u11() {
        return totIVApagnoacrimptas10u11;
    }

    public void setTotIVApagnoacrimptas10u11(Integer totIVApagnoacrimptas10u11) {
        this.totIVApagnoacrimptas10u11 = totIVApagnoacrimptas10u11;
    }

    public Integer getTotactpagimpbysnopagIVA() {
        return totactpagimpbysnopagIVA;
    }

    public void setTotactpagimpbysnopagIVA(Integer totactpagimpbysnopagIVA) {
        this.totactpagimpbysnopagIVA = totactpagimpbysnopagIVA;
    }

    public Integer getTotdemactpagtas0IVA() {
        return totdemactpagtas0IVA;
    }

    public void setTotdemactpagtas0IVA(Integer totdemactpagtas0IVA) {
        this.totdemactpagtas0IVA = totdemactpagtas0IVA;
    }

    public Integer getTotactpagnopagIVA() {
        return totactpagnopagIVA;
    }

    public void setTotactpagnopagIVA(Integer totactpagnopagIVA) {
        this.totactpagnopagIVA = totactpagnopagIVA;
    }

    public Integer getTotIVAretcont() {
        return totIVAretcont;
    }

    public void setTotIVAretcont(Integer totIVAretcont) {
        this.totIVAretcont = totIVAretcont;
    }

    public Integer getTotIVAdevdescybonifcomp() {
        return totIVAdevdescybonifcomp;
    }

    public void setTotIVAdevdescybonifcomp(Integer totIVAdevdescybonifcomp) {
        this.totIVAdevdescybonifcomp = totIVAdevdescybonifcomp;
    }

    public Integer getTotIVAtraslcontexcimpByS() {
        return totIVAtraslcontexcimpByS;
    }

    public void setTotIVAtraslcontexcimpByS(Integer totIVAtraslcontexcimpByS) {
        this.totIVAtraslcontexcimpByS = totIVAtraslcontexcimpByS;
    }

    public Integer getTotIVApagimpByS() {
        return totIVApagimpByS;
    }

    public void setTotIVApagimpByS(Integer totIVApagimpByS) {
        this.totIVApagimpByS = totIVApagimpByS;
    }

    public int getDPIVAidDPIVA() {
        return dPIVAidDPIVA;
    }

    public void setDPIVAidDPIVA(int dPIVAidDPIVA) {
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTOTALES != null ? idTOTALES.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Totales)) {
            return false;
        }
        Totales other = (Totales) object;
        if ((this.idTOTALES == null && other.idTOTALES != null) || (this.idTOTALES != null && !this.idTOTALES.equals(other.idTOTALES))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Totales[ idTOTALES=" + idTOTALES + " ]";
    }
    
}
