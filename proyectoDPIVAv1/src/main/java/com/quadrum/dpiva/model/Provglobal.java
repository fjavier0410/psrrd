/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "provglobal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provglobal.findAll", query = "SELECT p FROM Provglobal p"),
    @NamedQuery(name = "Provglobal.findByIdPROVGLOBAL", query = "SELECT p FROM Provglobal p WHERE p.idPROVGLOBAL = :idPROVGLOBAL"),
    @NamedQuery(name = "Provglobal.findByTipoOperac", query = "SELECT p FROM Provglobal p WHERE p.tipoOperac = :tipoOperac"),
    @NamedQuery(name = "Provglobal.findByValActPagTas15o16IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagTas15o16IVA = :valActPagTas15o16IVA"),
    @NamedQuery(name = "Provglobal.findByValActPagTas15IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagTas15IVA = :valActPagTas15IVA"),
    @NamedQuery(name = "Provglobal.findByMonIVAPagNoAcrTas15o16", query = "SELECT p FROM Provglobal p WHERE p.monIVAPagNoAcrTas15o16 = :monIVAPagNoAcrTas15o16"),
    @NamedQuery(name = "Provglobal.findByValActPagTas10u11IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagTas10u11IVA = :valActPagTas10u11IVA"),
    @NamedQuery(name = "Provglobal.findByValActPagTas10IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagTas10IVA = :valActPagTas10IVA"),
    @NamedQuery(name = "Provglobal.findByMonIVAPagNoAcrTas10u11", query = "SELECT p FROM Provglobal p WHERE p.monIVAPagNoAcrTas10u11 = :monIVAPagNoAcrTas10u11"),
    @NamedQuery(name = "Provglobal.findByValActPagImpBySTas15016IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagImpBySTas15016IVA = :valActPagImpBySTas15016IVA"),
    @NamedQuery(name = "Provglobal.findByMonIVAPagNoAcrImpTas15o16", query = "SELECT p FROM Provglobal p WHERE p.monIVAPagNoAcrImpTas15o16 = :monIVAPagNoAcrImpTas15o16"),
    @NamedQuery(name = "Provglobal.findByValActPagImpBySTas10u11IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagImpBySTas10u11IVA = :valActPagImpBySTas10u11IVA"),
    @NamedQuery(name = "Provglobal.findByMonIVAPagNoAcrImpTas10u11", query = "SELECT p FROM Provglobal p WHERE p.monIVAPagNoAcrImpTas10u11 = :monIVAPagNoAcrImpTas10u11"),
    @NamedQuery(name = "Provglobal.findByValActPagImpBySNoIVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagImpBySNoIVA = :valActPagImpBySNoIVA"),
    @NamedQuery(name = "Provglobal.findByValActPagTas0IVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagTas0IVA = :valActPagTas0IVA"),
    @NamedQuery(name = "Provglobal.findByValActPagNoIVA", query = "SELECT p FROM Provglobal p WHERE p.valActPagNoIVA = :valActPagNoIVA"),
    @NamedQuery(name = "Provglobal.findByIVARetCont", query = "SELECT p FROM Provglobal p WHERE p.iVARetCont = :iVARetCont"),
    @NamedQuery(name = "Provglobal.findByIVADevDescyBonifComp", query = "SELECT p FROM Provglobal p WHERE p.iVADevDescyBonifComp = :iVADevDescyBonifComp"),
    @NamedQuery(name = "Provglobal.findByDPIVAidDPIVA", query = "SELECT p FROM Provglobal p WHERE p.dPIVAidDPIVA = :dPIVAidDPIVA")})
public class Provglobal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPROVGLOBAL")
    private Integer idPROVGLOBAL;
    @Column(name = "TipoOperac")
    private Integer tipoOperac;
    @Column(name = "ValActPagTas15o16IVA")
    private Integer valActPagTas15o16IVA;
    @Column(name = "ValActPagTas15IVA")
    private Integer valActPagTas15IVA;
    @Column(name = "MonIVAPagNoAcrTas15o16")
    private Integer monIVAPagNoAcrTas15o16;
    @Column(name = "ValActPagTas10u11IVA")
    private Integer valActPagTas10u11IVA;
    @Column(name = "ValActPagTas10IVA")
    private Integer valActPagTas10IVA;
    @Column(name = "MonIVAPagNoAcrTas10u11")
    private Integer monIVAPagNoAcrTas10u11;
    @Column(name = "ValActPagImpBySTas15016IVA")
    private Integer valActPagImpBySTas15016IVA;
    @Column(name = "MonIVAPagNoAcrImpTas15o16")
    private Integer monIVAPagNoAcrImpTas15o16;
    @Column(name = "ValActPagImpBySTas10u11IVA")
    private Integer valActPagImpBySTas10u11IVA;
    @Column(name = "MonIVAPagNoAcrImpTas10u11")
    private Integer monIVAPagNoAcrImpTas10u11;
    @Column(name = "ValActPagImpBySNoIVA")
    private Integer valActPagImpBySNoIVA;
    @Column(name = "ValActPagTas0IVA")
    private Integer valActPagTas0IVA;
    @Column(name = "ValActPagNoIVA")
    private Integer valActPagNoIVA;
    @Column(name = "IVARetCont")
    private Integer iVARetCont;
    @Column(name = "IVADevDescyBonifComp")
    private Integer iVADevDescyBonifComp;
    @Basic(optional = false)
    @Column(name = "DPIVA_idDPIVA")
    private int dPIVAidDPIVA;

    public Provglobal() {
    }

    public Provglobal(Integer idPROVGLOBAL) {
        this.idPROVGLOBAL = idPROVGLOBAL;
    }

    public Provglobal(Integer idPROVGLOBAL, int dPIVAidDPIVA) {
        this.idPROVGLOBAL = idPROVGLOBAL;
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    public Integer getIdPROVGLOBAL() {
        return idPROVGLOBAL;
    }

    public void setIdPROVGLOBAL(Integer idPROVGLOBAL) {
        this.idPROVGLOBAL = idPROVGLOBAL;
    }

    public Integer getTipoOperac() {
        return tipoOperac;
    }

    public void setTipoOperac(Integer tipoOperac) {
        this.tipoOperac = tipoOperac;
    }

    public Integer getValActPagTas15o16IVA() {
        return valActPagTas15o16IVA;
    }

    public void setValActPagTas15o16IVA(Integer valActPagTas15o16IVA) {
        this.valActPagTas15o16IVA = valActPagTas15o16IVA;
    }

    public Integer getValActPagTas15IVA() {
        return valActPagTas15IVA;
    }

    public void setValActPagTas15IVA(Integer valActPagTas15IVA) {
        this.valActPagTas15IVA = valActPagTas15IVA;
    }

    public Integer getMonIVAPagNoAcrTas15o16() {
        return monIVAPagNoAcrTas15o16;
    }

    public void setMonIVAPagNoAcrTas15o16(Integer monIVAPagNoAcrTas15o16) {
        this.monIVAPagNoAcrTas15o16 = monIVAPagNoAcrTas15o16;
    }

    public Integer getValActPagTas10u11IVA() {
        return valActPagTas10u11IVA;
    }

    public void setValActPagTas10u11IVA(Integer valActPagTas10u11IVA) {
        this.valActPagTas10u11IVA = valActPagTas10u11IVA;
    }

    public Integer getValActPagTas10IVA() {
        return valActPagTas10IVA;
    }

    public void setValActPagTas10IVA(Integer valActPagTas10IVA) {
        this.valActPagTas10IVA = valActPagTas10IVA;
    }

    public Integer getMonIVAPagNoAcrTas10u11() {
        return monIVAPagNoAcrTas10u11;
    }

    public void setMonIVAPagNoAcrTas10u11(Integer monIVAPagNoAcrTas10u11) {
        this.monIVAPagNoAcrTas10u11 = monIVAPagNoAcrTas10u11;
    }

    public Integer getValActPagImpBySTas15016IVA() {
        return valActPagImpBySTas15016IVA;
    }

    public void setValActPagImpBySTas15016IVA(Integer valActPagImpBySTas15016IVA) {
        this.valActPagImpBySTas15016IVA = valActPagImpBySTas15016IVA;
    }

    public Integer getMonIVAPagNoAcrImpTas15o16() {
        return monIVAPagNoAcrImpTas15o16;
    }

    public void setMonIVAPagNoAcrImpTas15o16(Integer monIVAPagNoAcrImpTas15o16) {
        this.monIVAPagNoAcrImpTas15o16 = monIVAPagNoAcrImpTas15o16;
    }

    public Integer getValActPagImpBySTas10u11IVA() {
        return valActPagImpBySTas10u11IVA;
    }

    public void setValActPagImpBySTas10u11IVA(Integer valActPagImpBySTas10u11IVA) {
        this.valActPagImpBySTas10u11IVA = valActPagImpBySTas10u11IVA;
    }

    public Integer getMonIVAPagNoAcrImpTas10u11() {
        return monIVAPagNoAcrImpTas10u11;
    }

    public void setMonIVAPagNoAcrImpTas10u11(Integer monIVAPagNoAcrImpTas10u11) {
        this.monIVAPagNoAcrImpTas10u11 = monIVAPagNoAcrImpTas10u11;
    }

    public Integer getValActPagImpBySNoIVA() {
        return valActPagImpBySNoIVA;
    }

    public void setValActPagImpBySNoIVA(Integer valActPagImpBySNoIVA) {
        this.valActPagImpBySNoIVA = valActPagImpBySNoIVA;
    }

    public Integer getValActPagTas0IVA() {
        return valActPagTas0IVA;
    }

    public void setValActPagTas0IVA(Integer valActPagTas0IVA) {
        this.valActPagTas0IVA = valActPagTas0IVA;
    }

    public Integer getValActPagNoIVA() {
        return valActPagNoIVA;
    }

    public void setValActPagNoIVA(Integer valActPagNoIVA) {
        this.valActPagNoIVA = valActPagNoIVA;
    }

    public Integer getIVARetCont() {
        return iVARetCont;
    }

    public void setIVARetCont(Integer iVARetCont) {
        this.iVARetCont = iVARetCont;
    }

    public Integer getIVADevDescyBonifComp() {
        return iVADevDescyBonifComp;
    }

    public void setIVADevDescyBonifComp(Integer iVADevDescyBonifComp) {
        this.iVADevDescyBonifComp = iVADevDescyBonifComp;
    }

    public int getDPIVAidDPIVA() {
        return dPIVAidDPIVA;
    }

    public void setDPIVAidDPIVA(int dPIVAidDPIVA) {
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPROVGLOBAL != null ? idPROVGLOBAL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provglobal)) {
            return false;
        }
        Provglobal other = (Provglobal) object;
        if ((this.idPROVGLOBAL == null && other.idPROVGLOBAL != null) || (this.idPROVGLOBAL != null && !this.idPROVGLOBAL.equals(other.idPROVGLOBAL))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Provglobal[ idPROVGLOBAL=" + idPROVGLOBAL + " ]";
    }
    
}
