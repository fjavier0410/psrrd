package com.quadrum.dpiva.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;


public class Certificados {
	
    @Getter @Setter private MultipartFile cer;
    @Getter @Setter private MultipartFile key;

}