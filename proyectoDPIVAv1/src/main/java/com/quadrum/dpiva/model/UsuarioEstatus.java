package com.quadrum.dpiva.model;

public enum UsuarioEstatus {
	ACTIVE,
	INACTIVE;
}
