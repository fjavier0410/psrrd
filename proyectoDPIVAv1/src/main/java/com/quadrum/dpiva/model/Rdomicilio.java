/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "rdomicilio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rdomicilio.findAll", query = "SELECT r FROM Rdomicilio r"),
    @NamedQuery(name = "Rdomicilio.findByIdRDOMICILIO", query = "SELECT r FROM Rdomicilio r WHERE r.idRDOMICILIO = :idRDOMICILIO"),
    @NamedQuery(name = "Rdomicilio.findByRcalle", query = "SELECT r FROM Rdomicilio r WHERE r.rcalle = :rcalle"),
    @NamedQuery(name = "Rdomicilio.findByRnumext", query = "SELECT r FROM Rdomicilio r WHERE r.rnumext = :rnumext"),
    @NamedQuery(name = "Rdomicilio.findByRnumint", query = "SELECT r FROM Rdomicilio r WHERE r.rnumint = :rnumint"),
    @NamedQuery(name = "Rdomicilio.findByRcolonia", query = "SELECT r FROM Rdomicilio r WHERE r.rcolonia = :rcolonia"),
    @NamedQuery(name = "Rdomicilio.findByRlocalidad", query = "SELECT r FROM Rdomicilio r WHERE r.rlocalidad = :rlocalidad"),
    @NamedQuery(name = "Rdomicilio.findByRref", query = "SELECT r FROM Rdomicilio r WHERE r.rref = :rref"),
    @NamedQuery(name = "Rdomicilio.findByRnumdel", query = "SELECT r FROM Rdomicilio r WHERE r.rnumdel = :rnumdel"),
    @NamedQuery(name = "Rdomicilio.findByRentidadf", query = "SELECT r FROM Rdomicilio r WHERE r.rentidadf = :rentidadf"),
    @NamedQuery(name = "Rdomicilio.findByRmundel", query = "SELECT r FROM Rdomicilio r WHERE r.rmundel = :rmundel"),
    @NamedQuery(name = "Rdomicilio.findByRpais", query = "SELECT r FROM Rdomicilio r WHERE r.rpais = :rpais"),
    @NamedQuery(name = "Rdomicilio.findByRcp", query = "SELECT r FROM Rdomicilio r WHERE r.rcp = :rcp")})
public class Rdomicilio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idRDOMICILIO")
    private Integer idRDOMICILIO;
    @Column(name = "rcalle")
    private String rcalle;
    @Column(name = "rnumext")
    private String rnumext;
    @Column(name = "rnumint")
    private String rnumint;
    @Column(name = "rcolonia")
    private String rcolonia;
    @Column(name = "rlocalidad")
    private String rlocalidad;
    @Column(name = "rref")
    private String rref;
    @Column(name = "rnumdel")
    private String rnumdel;
    @Column(name = "rentidadf")
    private String rentidadf;
    @Column(name = "rmundel")
    private String rmundel;
    @Column(name = "rpais")
    private String rpais;
    @Column(name = "rcp")
    private String rcp;

    public Rdomicilio() {
    }

    public Rdomicilio(Integer idRDOMICILIO) {
        this.idRDOMICILIO = idRDOMICILIO;
    }

    public Integer getIdRDOMICILIO() {
        return idRDOMICILIO;
    }

    public void setIdRDOMICILIO(Integer idRDOMICILIO) {
        this.idRDOMICILIO = idRDOMICILIO;
    }

    public String getRcalle() {
        return rcalle;
    }

    public void setRcalle(String rcalle) {
        this.rcalle = rcalle;
    }

    public String getRnumext() {
        return rnumext;
    }

    public void setRnumext(String rnumext) {
        this.rnumext = rnumext;
    }

    public String getRnumint() {
        return rnumint;
    }

    public void setRnumint(String rnumint) {
        this.rnumint = rnumint;
    }

    public String getRcolonia() {
        return rcolonia;
    }

    public void setRcolonia(String rcolonia) {
        this.rcolonia = rcolonia;
    }

    public String getRlocalidad() {
        return rlocalidad;
    }

    public void setRlocalidad(String rlocalidad) {
        this.rlocalidad = rlocalidad;
    }

    public String getRref() {
        return rref;
    }

    public void setRref(String rref) {
        this.rref = rref;
    }

    public String getRnumdel() {
        return rnumdel;
    }

    public void setRnumdel(String rnumdel) {
        this.rnumdel = rnumdel;
    }

    public String getRentidadf() {
        return rentidadf;
    }

    public void setRentidadf(String rentidadf) {
        this.rentidadf = rentidadf;
    }

    public String getRmundel() {
        return rmundel;
    }

    public void setRmundel(String rmundel) {
        this.rmundel = rmundel;
    }

    public String getRpais() {
        return rpais;
    }

    public void setRpais(String rpais) {
        this.rpais = rpais;
    }

    public String getRcp() {
        return rcp;
    }

    public void setRcp(String rcp) {
        this.rcp = rcp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRDOMICILIO != null ? idRDOMICILIO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rdomicilio)) {
            return false;
        }
        Rdomicilio other = (Rdomicilio) object;
        if ((this.idRDOMICILIO == null && other.idRDOMICILIO != null) || (this.idRDOMICILIO != null && !this.idRDOMICILIO.equals(other.idRDOMICILIO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Rdomicilio[ idRDOMICILIO=" + idRDOMICILIO + " ]";
    }
    
}
