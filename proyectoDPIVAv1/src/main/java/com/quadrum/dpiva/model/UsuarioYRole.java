/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "usuario_y_role")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioYRole.findAll", query = "SELECT u FROM UsuarioYRole u"),
    @NamedQuery(name = "UsuarioYRole.findByIdusuarioYRole", query = "SELECT u FROM UsuarioYRole u WHERE u.idusuarioYRole = :idusuarioYRole")})
public class UsuarioYRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idusuario_y_role")
    private Integer idusuarioYRole;
    @JoinColumn(name = "usuario_id", referencedColumnName = "idusuario")
    @ManyToOne
    private Usuario usuarioId;
    @JoinColumn(name = "role_id", referencedColumnName = "idrole")
    @ManyToOne
    private Role roleId;

    public UsuarioYRole() {
    }

    public UsuarioYRole(Integer idusuarioYRole) {
        this.idusuarioYRole = idusuarioYRole;
    }

    public Integer getIdusuarioYRole() {
        return idusuarioYRole;
    }

    public void setIdusuarioYRole(Integer idusuarioYRole) {
        this.idusuarioYRole = idusuarioYRole;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Role getRoleId() {
        return roleId;
    }

    public void setRoleId(Role roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idusuarioYRole != null ? idusuarioYRole.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioYRole)) {
            return false;
        }
        UsuarioYRole other = (UsuarioYRole) object;
        if ((this.idusuarioYRole == null && other.idusuarioYRole != null) || (this.idusuarioYRole != null && !this.idusuarioYRole.equals(other.idusuarioYRole))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.UsuarioYRole[ idusuarioYRole=" + idusuarioYRole + " ]";
    }
    
}
