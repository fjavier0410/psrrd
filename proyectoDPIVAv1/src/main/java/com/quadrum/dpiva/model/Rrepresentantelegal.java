/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "rrepresentantelegal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rrepresentantelegal.findAll", query = "SELECT r FROM Rrepresentantelegal r"),
    @NamedQuery(name = "Rrepresentantelegal.findByIdRREPRESENTANTELEGAL", query = "SELECT r FROM Rrepresentantelegal r WHERE r.idRREPRESENTANTELEGAL = :idRREPRESENTANTELEGAL"),
    @NamedQuery(name = "Rrepresentantelegal.findByRrfcrep", query = "SELECT r FROM Rrepresentantelegal r WHERE r.rrfcrep = :rrfcrep"),
    @NamedQuery(name = "Rrepresentantelegal.findByRcurprep", query = "SELECT r FROM Rrepresentantelegal r WHERE r.rcurprep = :rcurprep"),
    @NamedQuery(name = "Rrepresentantelegal.findByRapellpatrep", query = "SELECT r FROM Rrepresentantelegal r WHERE r.rapellpatrep = :rapellpatrep"),
    @NamedQuery(name = "Rrepresentantelegal.findByRapellmatrep", query = "SELECT r FROM Rrepresentantelegal r WHERE r.rapellmatrep = :rapellmatrep"),
    @NamedQuery(name = "Rrepresentantelegal.findByRnombrerep", query = "SELECT r FROM Rrepresentantelegal r WHERE r.rnombrerep = :rnombrerep")})
public class Rrepresentantelegal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRREPRESENTANTELEGAL")
    private Integer idRREPRESENTANTELEGAL;
    @Column(name = "rrfcrep")
    private String rrfcrep;
    @Column(name = "rcurprep")
    private String rcurprep;
    @Column(name = "rapellpatrep")
    private String rapellpatrep;
    @Column(name = "rapellmatrep")
    private String rapellmatrep;
    @Column(name = "rnombrerep")
    private String rnombrerep;

    public Rrepresentantelegal() {
    }

    public Rrepresentantelegal(Integer idRREPRESENTANTELEGAL) {
        this.idRREPRESENTANTELEGAL = idRREPRESENTANTELEGAL;
    }

    public Integer getIdRREPRESENTANTELEGAL() {
        return idRREPRESENTANTELEGAL;
    }

    public void setIdRREPRESENTANTELEGAL(Integer idRREPRESENTANTELEGAL) {
        this.idRREPRESENTANTELEGAL = idRREPRESENTANTELEGAL;
    }

    public String getRrfcrep() {
        return rrfcrep;
    }

    public void setRrfcrep(String rrfcrep) {
        this.rrfcrep = rrfcrep;
    }

    public String getRcurprep() {
        return rcurprep;
    }

    public void setRcurprep(String rcurprep) {
        this.rcurprep = rcurprep;
    }

    public String getRapellpatrep() {
        return rapellpatrep;
    }

    public void setRapellpatrep(String rapellpatrep) {
        this.rapellpatrep = rapellpatrep;
    }

    public String getRapellmatrep() {
        return rapellmatrep;
    }

    public void setRapellmatrep(String rapellmatrep) {
        this.rapellmatrep = rapellmatrep;
    }

    public String getRnombrerep() {
        return rnombrerep;
    }

    public void setRnombrerep(String rnombrerep) {
        this.rnombrerep = rnombrerep;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRREPRESENTANTELEGAL != null ? idRREPRESENTANTELEGAL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rrepresentantelegal)) {
            return false;
        }
        Rrepresentantelegal other = (Rrepresentantelegal) object;
        if ((this.idRREPRESENTANTELEGAL == null && other.idRREPRESENTANTELEGAL != null) || (this.idRREPRESENTANTELEGAL != null && !this.idRREPRESENTANTELEGAL.equals(other.idRREPRESENTANTELEGAL))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Rrepresentantelegal[ idRREPRESENTANTELEGAL=" + idRREPRESENTANTELEGAL + " ]";
    }
    
}
