/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "provextranjero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provextranjero.findAll", query = "SELECT p FROM Provextranjero p"),
    @NamedQuery(name = "Provextranjero.findByIdPROVEXTRANJERO", query = "SELECT p FROM Provextranjero p WHERE p.idPROVEXTRANJERO = :idPROVEXTRANJERO"),
    @NamedQuery(name = "Provextranjero.findByTipoOperac", query = "SELECT p FROM Provextranjero p WHERE p.tipoOperac = :tipoOperac"),
    @NamedQuery(name = "Provextranjero.findByRFCProv", query = "SELECT p FROM Provextranjero p WHERE p.rFCProv = :rFCProv"),
    @NamedQuery(name = "Provextranjero.findByNumIDFiscal", query = "SELECT p FROM Provextranjero p WHERE p.numIDFiscal = :numIDFiscal"),
    @NamedQuery(name = "Provextranjero.findByNombExtranjero", query = "SELECT p FROM Provextranjero p WHERE p.nombExtranjero = :nombExtranjero"),
    @NamedQuery(name = "Provextranjero.findByPaisResidencia", query = "SELECT p FROM Provextranjero p WHERE p.paisResidencia = :paisResidencia"),
    @NamedQuery(name = "Provextranjero.findByNacionalidad", query = "SELECT p FROM Provextranjero p WHERE p.nacionalidad = :nacionalidad"),
    @NamedQuery(name = "Provextranjero.findByValActPagTas15o16IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagTas15o16IVA = :valActPagTas15o16IVA"),
    @NamedQuery(name = "Provextranjero.findByValActPagTas15IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagTas15IVA = :valActPagTas15IVA"),
    @NamedQuery(name = "Provextranjero.findByMonIVAPagNoAcrTas15o16", query = "SELECT p FROM Provextranjero p WHERE p.monIVAPagNoAcrTas15o16 = :monIVAPagNoAcrTas15o16"),
    @NamedQuery(name = "Provextranjero.findByValActPagTas10u11IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagTas10u11IVA = :valActPagTas10u11IVA"),
    @NamedQuery(name = "Provextranjero.findByValActPagTas10IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagTas10IVA = :valActPagTas10IVA"),
    @NamedQuery(name = "Provextranjero.findByMonIVAPagNoAcrTas10u11", query = "SELECT p FROM Provextranjero p WHERE p.monIVAPagNoAcrTas10u11 = :monIVAPagNoAcrTas10u11"),
    @NamedQuery(name = "Provextranjero.findByValActPagImpBySTas15016IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagImpBySTas15016IVA = :valActPagImpBySTas15016IVA"),
    @NamedQuery(name = "Provextranjero.findByMonIVAPagNoAcrImpTas15o16", query = "SELECT p FROM Provextranjero p WHERE p.monIVAPagNoAcrImpTas15o16 = :monIVAPagNoAcrImpTas15o16"),
    @NamedQuery(name = "Provextranjero.findByValActPagImpBySTas10u11IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagImpBySTas10u11IVA = :valActPagImpBySTas10u11IVA"),
    @NamedQuery(name = "Provextranjero.findByMonIVAPagNoAcrImpTas10u11", query = "SELECT p FROM Provextranjero p WHERE p.monIVAPagNoAcrImpTas10u11 = :monIVAPagNoAcrImpTas10u11"),
    @NamedQuery(name = "Provextranjero.findByValActPagImpBySNoIVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagImpBySNoIVA = :valActPagImpBySNoIVA"),
    @NamedQuery(name = "Provextranjero.findByValActPagTas0IVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagTas0IVA = :valActPagTas0IVA"),
    @NamedQuery(name = "Provextranjero.findByValActPagNoIVA", query = "SELECT p FROM Provextranjero p WHERE p.valActPagNoIVA = :valActPagNoIVA"),
    @NamedQuery(name = "Provextranjero.findByIVARetCont", query = "SELECT p FROM Provextranjero p WHERE p.iVARetCont = :iVARetCont"),
    @NamedQuery(name = "Provextranjero.findByIVADevDescyBonifComp", query = "SELECT p FROM Provextranjero p WHERE p.iVADevDescyBonifComp = :iVADevDescyBonifComp"),
    @NamedQuery(name = "Provextranjero.findByDPIVAidDPIVA", query = "SELECT p FROM Provextranjero p WHERE p.dPIVAidDPIVA = :dPIVAidDPIVA")})
public class Provextranjero implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPROVEXTRANJERO")
    private Integer idPROVEXTRANJERO;
    @Column(name = "TipoOperac")
    private String tipoOperac;
    @Column(name = "RFCProv")
    private String rFCProv;
    @Column(name = "NumIDFiscal")
    private String numIDFiscal;
    @Column(name = "NombExtranjero")
    private String nombExtranjero;
    @Column(name = "PaisResidencia")
    private String paisResidencia;
    @Column(name = "Nacionalidad")
    private String nacionalidad;
    @Column(name = "ValActPagTas15o16IVA")
    private String valActPagTas15o16IVA;
    @Column(name = "ValActPagTas15IVA")
    private String valActPagTas15IVA;
    @Column(name = "MonIVAPagNoAcrTas15o16")
    private String monIVAPagNoAcrTas15o16;
    @Column(name = "ValActPagTas10u11IVA")
    private String valActPagTas10u11IVA;
    @Column(name = "ValActPagTas10IVA")
    private String valActPagTas10IVA;
    @Column(name = "MonIVAPagNoAcrTas10u11")
    private String monIVAPagNoAcrTas10u11;
    @Column(name = "ValActPagImpBySTas15016IVA")
    private String valActPagImpBySTas15016IVA;
    @Column(name = "MonIVAPagNoAcrImpTas15o16")
    private String monIVAPagNoAcrImpTas15o16;
    @Column(name = "ValActPagImpBySTas10u11IVA")
    private String valActPagImpBySTas10u11IVA;
    @Column(name = "MonIVAPagNoAcrImpTas10u11")
    private String monIVAPagNoAcrImpTas10u11;
    @Column(name = "ValActPagImpBySNoIVA")
    private String valActPagImpBySNoIVA;
    @Column(name = "ValActPagTas0IVA")
    private String valActPagTas0IVA;
    @Column(name = "ValActPagNoIVA")
    private String valActPagNoIVA;
    @Column(name = "IVARetCont")
    private String iVARetCont;
    @Column(name = "IVADevDescyBonifComp")
    private String iVADevDescyBonifComp;
    @Basic(optional = false)
    @Column(name = "DPIVA_idDPIVA")
    private int dPIVAidDPIVA;

    public Provextranjero() {
    }

    public Provextranjero(Integer idPROVEXTRANJERO) {
        this.idPROVEXTRANJERO = idPROVEXTRANJERO;
    }

    public Provextranjero(Integer idPROVEXTRANJERO, int dPIVAidDPIVA) {
        this.idPROVEXTRANJERO = idPROVEXTRANJERO;
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    public Integer getIdPROVEXTRANJERO() {
        return idPROVEXTRANJERO;
    }

    public void setIdPROVEXTRANJERO(Integer idPROVEXTRANJERO) {
        this.idPROVEXTRANJERO = idPROVEXTRANJERO;
    }

    public String getTipoOperac() {
        return tipoOperac;
    }

    public void setTipoOperac(String tipoOperac) {
        this.tipoOperac = tipoOperac;
    }

    public String getRFCProv() {
        return rFCProv;
    }

    public void setRFCProv(String rFCProv) {
        this.rFCProv = rFCProv;
    }

    public String getNumIDFiscal() {
        return numIDFiscal;
    }

    public void setNumIDFiscal(String numIDFiscal) {
        this.numIDFiscal = numIDFiscal;
    }

    public String getNombExtranjero() {
        return nombExtranjero;
    }

    public void setNombExtranjero(String nombExtranjero) {
        this.nombExtranjero = nombExtranjero;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getValActPagTas15o16IVA() {
        return valActPagTas15o16IVA;
    }

    public void setValActPagTas15o16IVA(String valActPagTas15o16IVA) {
        this.valActPagTas15o16IVA = valActPagTas15o16IVA;
    }

    public String getValActPagTas15IVA() {
        return valActPagTas15IVA;
    }

    public void setValActPagTas15IVA(String valActPagTas15IVA) {
        this.valActPagTas15IVA = valActPagTas15IVA;
    }

    public String getMonIVAPagNoAcrTas15o16() {
        return monIVAPagNoAcrTas15o16;
    }

    public void setMonIVAPagNoAcrTas15o16(String monIVAPagNoAcrTas15o16) {
        this.monIVAPagNoAcrTas15o16 = monIVAPagNoAcrTas15o16;
    }

    public String getValActPagTas10u11IVA() {
        return valActPagTas10u11IVA;
    }

    public void setValActPagTas10u11IVA(String valActPagTas10u11IVA) {
        this.valActPagTas10u11IVA = valActPagTas10u11IVA;
    }

    public String getValActPagTas10IVA() {
        return valActPagTas10IVA;
    }

    public void setValActPagTas10IVA(String valActPagTas10IVA) {
        this.valActPagTas10IVA = valActPagTas10IVA;
    }

    public String getMonIVAPagNoAcrTas10u11() {
        return monIVAPagNoAcrTas10u11;
    }

    public void setMonIVAPagNoAcrTas10u11(String monIVAPagNoAcrTas10u11) {
        this.monIVAPagNoAcrTas10u11 = monIVAPagNoAcrTas10u11;
    }

    public String getValActPagImpBySTas15016IVA() {
        return valActPagImpBySTas15016IVA;
    }

    public void setValActPagImpBySTas15016IVA(String valActPagImpBySTas15016IVA) {
        this.valActPagImpBySTas15016IVA = valActPagImpBySTas15016IVA;
    }

    public String getMonIVAPagNoAcrImpTas15o16() {
        return monIVAPagNoAcrImpTas15o16;
    }

    public void setMonIVAPagNoAcrImpTas15o16(String monIVAPagNoAcrImpTas15o16) {
        this.monIVAPagNoAcrImpTas15o16 = monIVAPagNoAcrImpTas15o16;
    }

    public String getValActPagImpBySTas10u11IVA() {
        return valActPagImpBySTas10u11IVA;
    }

    public void setValActPagImpBySTas10u11IVA(String valActPagImpBySTas10u11IVA) {
        this.valActPagImpBySTas10u11IVA = valActPagImpBySTas10u11IVA;
    }

    public String getMonIVAPagNoAcrImpTas10u11() {
        return monIVAPagNoAcrImpTas10u11;
    }

    public void setMonIVAPagNoAcrImpTas10u11(String monIVAPagNoAcrImpTas10u11) {
        this.monIVAPagNoAcrImpTas10u11 = monIVAPagNoAcrImpTas10u11;
    }

    public String getValActPagImpBySNoIVA() {
        return valActPagImpBySNoIVA;
    }

    public void setValActPagImpBySNoIVA(String valActPagImpBySNoIVA) {
        this.valActPagImpBySNoIVA = valActPagImpBySNoIVA;
    }

    public String getValActPagTas0IVA() {
        return valActPagTas0IVA;
    }

    public void setValActPagTas0IVA(String valActPagTas0IVA) {
        this.valActPagTas0IVA = valActPagTas0IVA;
    }

    public String getValActPagNoIVA() {
        return valActPagNoIVA;
    }

    public void setValActPagNoIVA(String valActPagNoIVA) {
        this.valActPagNoIVA = valActPagNoIVA;
    }

    public String getIVARetCont() {
        return iVARetCont;
    }

    public void setIVARetCont(String iVARetCont) {
        this.iVARetCont = iVARetCont;
    }

    public String getIVADevDescyBonifComp() {
        return iVADevDescyBonifComp;
    }

    public void setIVADevDescyBonifComp(String iVADevDescyBonifComp) {
        this.iVADevDescyBonifComp = iVADevDescyBonifComp;
    }

    public int getDPIVAidDPIVA() {
        return dPIVAidDPIVA;
    }

    public void setDPIVAidDPIVA(int dPIVAidDPIVA) {
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPROVEXTRANJERO != null ? idPROVEXTRANJERO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provextranjero)) {
            return false;
        }
        Provextranjero other = (Provextranjero) object;
        if ((this.idPROVEXTRANJERO == null && other.idPROVEXTRANJERO != null) || (this.idPROVEXTRANJERO != null && !this.idPROVEXTRANJERO.equals(other.idPROVEXTRANJERO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Provextranjero[ idPROVEXTRANJERO=" + idPROVEXTRANJERO + " ]";
    }
    
}
