/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "rempleadode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rempleadode.findAll", query = "SELECT r FROM Rempleadode r"),
    @NamedQuery(name = "Rempleadode.findByIdREMPLEADODE", query = "SELECT r FROM Rempleadode r WHERE r.idREMPLEADODE = :idREMPLEADODE"),
    @NamedQuery(name = "Rempleadode.findByRrfcorg", query = "SELECT r FROM Rempleadode r WHERE r.rrfcorg = :rrfcorg"),
    @NamedQuery(name = "Rempleadode.findByRdenorazsocorg", query = "SELECT r FROM Rempleadode r WHERE r.rdenorazsocorg = :rdenorazsocorg")})
public class Rempleadode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idREMPLEADODE")
    private Integer idREMPLEADODE;
    @Column(name = "rrfcorg")
    private String rrfcorg;
    @Column(name = "rdenorazsocorg")
    private String rdenorazsocorg;

    public Rempleadode() {
    }

    public Rempleadode(Integer idREMPLEADODE) {
        this.idREMPLEADODE = idREMPLEADODE;
    }

    public Integer getIdREMPLEADODE() {
        return idREMPLEADODE;
    }

    public void setIdREMPLEADODE(Integer idREMPLEADODE) {
        this.idREMPLEADODE = idREMPLEADODE;
    }

    public String getRrfcorg() {
        return rrfcorg;
    }

    public void setRrfcorg(String rrfcorg) {
        this.rrfcorg = rrfcorg;
    }

    public String getRdenorazsocorg() {
        return rdenorazsocorg;
    }

    public void setRdenorazsocorg(String rdenorazsocorg) {
        this.rdenorazsocorg = rdenorazsocorg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idREMPLEADODE != null ? idREMPLEADODE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rempleadode)) {
            return false;
        }
        Rempleadode other = (Rempleadode) object;
        if ((this.idREMPLEADODE == null && other.idREMPLEADODE != null) || (this.idREMPLEADODE != null && !this.idREMPLEADODE.equals(other.idREMPLEADODE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Rempleadode[ idREMPLEADODE=" + idREMPLEADODE + " ]";
    }
    
}
