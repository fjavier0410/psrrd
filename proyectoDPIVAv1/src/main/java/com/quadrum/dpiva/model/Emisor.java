/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.web.multipart.MultipartFile;

import com.quadrum.dpiva.model.Usuario;;
/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "emisor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Emisor.findAll", query = "SELECT e FROM Emisor e"),
    @NamedQuery(name = "Emisor.findByIdEMISOR", query = "SELECT e FROM Emisor e WHERE e.idEMISOR = :idEMISOR"),
    @NamedQuery(name = "Emisor.findByErfc", query = "SELECT e FROM Emisor e WHERE e.erfc = :erfc"),
    @NamedQuery(name = "Emisor.findByEcurp", query = "SELECT e FROM Emisor e WHERE e.ecurp = :ecurp"),
    @NamedQuery(name = "Emisor.findByEapellpat", query = "SELECT e FROM Emisor e WHERE e.eapellpat = :eapellpat"),
    @NamedQuery(name = "Emisor.findByEapellmat", query = "SELECT e FROM Emisor e WHERE e.eapellmat = :eapellmat"),
    @NamedQuery(name = "Emisor.findByEnombre", query = "SELECT e FROM Emisor e WHERE e.enombre = :enombre"),
    @NamedQuery(name = "Emisor.findByEdenrazsoc", query = "SELECT e FROM Emisor e WHERE e.edenrazsoc = :edenrazsoc"),
    @NamedQuery(name = "Emisor.findByEcorreoe", query = "SELECT e FROM Emisor e WHERE e.ecorreoe = :ecorreoe"),
    @NamedQuery(name = "Emisor.findByEnumtel", query = "SELECT e FROM Emisor e WHERE e.enumtel = :enumtel"),
    @NamedQuery(name = "Emisor.findByEEMPLEADODEidEEMPLEADODE", query = "SELECT e FROM Emisor e WHERE e.eEMPLEADODEidEEMPLEADODE = :eEMPLEADODEidEEMPLEADODE"),
    @NamedQuery(name = "Emisor.findByEDOMICILIOidEDOMICILIO", query = "SELECT e FROM Emisor e WHERE e.eDOMICILIOidEDOMICILIO = :eDOMICILIOidEDOMICILIO"),
    @NamedQuery(name = "Emisor.findByEREPRESENTANTELEGALidEREPRESENTANTELEGAL", query = "SELECT e FROM Emisor e WHERE e.eREPRESENTANTELEGALidEREPRESENTANTELEGAL = :eREPRESENTANTELEGALidEREPRESENTANTELEGAL")})
public class Emisor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEMISOR")
    private Integer idEMISOR;
    @Column(name = "erfc")
    private String erfc;
    @Column(name = "ecurp")
    private String ecurp;
    @Column(name = "eapellpat")
    private String eapellpat;
    @Column(name = "eapellmat")
    private String eapellmat;
    @Column(name = "enombre")
    private String enombre;
    @Column(name = "edenrazsoc")
    private String edenrazsoc;
    @Column(name = "ecorreoe")
    private String ecorreoe;
    @Column(name = "enumtel")
    private String enumtel;
    
    @Column(name = "fileCer")
    private byte [] fileCer; 
    
    @Column(name = "fileKey")
    private byte [] fileKey; 
    
    @Column(name="passwordKey")
    private String passwordKey;
    
    @Basic(optional = false)
    @Column(name = "EEMPLEADODE_idEEMPLEADODE")
    private int eEMPLEADODEidEEMPLEADODE;
    @Basic(optional = false)
    @Column(name = "EDOMICILIO_idEDOMICILIO")
    private int eDOMICILIOidEDOMICILIO;
    @Basic(optional = false)
    @Column(name = "EREPRESENTANTELEGAL_idEREPRESENTANTELEGAL")
    private int eREPRESENTANTELEGALidEREPRESENTANTELEGAL;
    
  
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario")
    @ManyToOne
    private Usuario idusuario;

    
    
   

	public Emisor() {
    }

    public Emisor(Integer idEMISOR) {
        this.idEMISOR = idEMISOR;
    }

    public Emisor(Integer idEMISOR, int eEMPLEADODEidEEMPLEADODE, int eDOMICILIOidEDOMICILIO, int eREPRESENTANTELEGALidEREPRESENTANTELEGAL) {
        this.idEMISOR = idEMISOR;
        this.eEMPLEADODEidEEMPLEADODE = eEMPLEADODEidEEMPLEADODE;
        this.eDOMICILIOidEDOMICILIO = eDOMICILIOidEDOMICILIO;
        this.eREPRESENTANTELEGALidEREPRESENTANTELEGAL = eREPRESENTANTELEGALidEREPRESENTANTELEGAL;
    }

    public Integer getIdEMISOR() {
        return idEMISOR;
    }

    public void setIdEMISOR(Integer idEMISOR) {
        this.idEMISOR = idEMISOR;
    }

    public String getErfc() {
        return erfc;
    }

    public void setErfc(String erfc) {
        this.erfc = erfc;
    }

    public String getEcurp() {
        return ecurp;
    }

    public void setEcurp(String ecurp) {
        this.ecurp = ecurp;
    }

    public String getEapellpat() {
        return eapellpat;
    }

    public void setEapellpat(String eapellpat) {
        this.eapellpat = eapellpat;
    }

    public String getEapellmat() {
        return eapellmat;
    }

    public void setEapellmat(String eapellmat) {
        this.eapellmat = eapellmat;
    }

    public String getEnombre() {
        return enombre;
    }

    public void setEnombre(String enombre) {
        this.enombre = enombre;
    }

    public String getEdenrazsoc() {
        return edenrazsoc;
    }

    public void setEdenrazsoc(String edenrazsoc) {
        this.edenrazsoc = edenrazsoc;
    }

    public String getEcorreoe() {
        return ecorreoe;
    }

    public void setEcorreoe(String ecorreoe) {
        this.ecorreoe = ecorreoe;
    }

    public String getEnumtel() {
        return enumtel;
    }

    public void setEnumtel(String enumtel) {
        this.enumtel = enumtel;
    }
    

    public byte[] getFileCer() {
		return fileCer;
	}

	public void setFileCer(byte[] fileCer) {
		this.fileCer = fileCer;
	}

	public byte[] getFileKey() {
		return fileKey;
	}

	public void setFileKey(byte[] fileKey) {
		this.fileKey = fileKey;
	}
	
	

	public String getPasswordKey() {
		return passwordKey;
	}

	public void setPasswordKey(String passwordKey) {
		this.passwordKey = passwordKey;
	}

	public int getEEMPLEADODEidEEMPLEADODE() {
        return eEMPLEADODEidEEMPLEADODE;
    }

    public void setEEMPLEADODEidEEMPLEADODE(int eEMPLEADODEidEEMPLEADODE) {
        this.eEMPLEADODEidEEMPLEADODE = eEMPLEADODEidEEMPLEADODE;
    }

    public int getEDOMICILIOidEDOMICILIO() {
        return eDOMICILIOidEDOMICILIO;
    }

    public void setEDOMICILIOidEDOMICILIO(int eDOMICILIOidEDOMICILIO) {
        this.eDOMICILIOidEDOMICILIO = eDOMICILIOidEDOMICILIO;
    }

    public int getEREPRESENTANTELEGALidEREPRESENTANTELEGAL() {
        return eREPRESENTANTELEGALidEREPRESENTANTELEGAL;
    }

    public void setEREPRESENTANTELEGALidEREPRESENTANTELEGAL(int eREPRESENTANTELEGALidEREPRESENTANTELEGAL) {
        this.eREPRESENTANTELEGALidEREPRESENTANTELEGAL = eREPRESENTANTELEGALidEREPRESENTANTELEGAL;
    }
    
    public Usuario getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Usuario idusuario) {
		this.idusuario = idusuario;
	}
  
	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idEMISOR != null ? idEMISOR.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Emisor)) {
            return false;
        }
        Emisor other = (Emisor) object;
        if ((this.idEMISOR == null && other.idEMISOR != null) || (this.idEMISOR != null && !this.idEMISOR.equals(other.idEMISOR))) {
            return false;
        }
        return true;
    }

    
    
    
	@Override
    public String toString() {
        return "demodpiva.Emisor[ idEMISOR=" + idEMISOR + " ]";
    }
    
}
