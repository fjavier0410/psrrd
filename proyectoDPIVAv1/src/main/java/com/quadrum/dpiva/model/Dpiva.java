/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "dpiva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dpiva.findAll", query = "SELECT d FROM Dpiva d"),
    @NamedQuery(name = "Dpiva.findByIdDPIVA", query = "SELECT d FROM Dpiva d WHERE d.idDPIVA = :idDPIVA"),
    @NamedQuery(name = "Dpiva.findByVersion", query = "SELECT d FROM Dpiva d WHERE d.version = :version"),
    @NamedQuery(name = "Dpiva.findByTipoinfo", query = "SELECT d FROM Dpiva d WHERE d.tipoinfo = :tipoinfo"),
    @NamedQuery(name = "Dpiva.findByFolioant", query = "SELECT d FROM Dpiva d WHERE d.folioant = :folioant"),
    @NamedQuery(name = "Dpiva.findByFechpresant", query = "SELECT d FROM Dpiva d WHERE d.fechpresant = :fechpresant")})
public class Dpiva implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDPIVA")
    private Integer idDPIVA;
    @Column(name = "Version")
    private String version;
    @Column(name = "tipoinfo")
    private String tipoinfo;
    @Column(name = "folioant")
    private String folioant;
    @Column(name = "fechpresant")
    @Temporal(TemporalType.DATE)
    private Date fechpresant;

    public Dpiva() {
    }

    public Dpiva(Integer idDPIVA) {
        this.idDPIVA = idDPIVA;
    }

    public Integer getIdDPIVA() {
        return idDPIVA;
    }

    public void setIdDPIVA(Integer idDPIVA) {
        this.idDPIVA = idDPIVA;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTipoinfo() {
        return tipoinfo;
    }

    public void setTipoinfo(String tipoinfo) {
        this.tipoinfo = tipoinfo;
    }

    public String getFolioant() {
        return folioant;
    }

    public void setFolioant(String folioant) {
        this.folioant = folioant;
    }

    public Date getFechpresant() {
        return fechpresant;
    }

    public void setFechpresant(Date fechpresant) {
        this.fechpresant = fechpresant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDPIVA != null ? idDPIVA.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dpiva)) {
            return false;
        }
        Dpiva other = (Dpiva) object;
        if ((this.idDPIVA == null && other.idDPIVA != null) || (this.idDPIVA != null && !this.idDPIVA.equals(other.idDPIVA))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Dpiva[ idDPIVA=" + idDPIVA + " ]";
    }
    
}
