/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "provnacional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provnacional.findAll", query = "SELECT p FROM Provnacional p"),
    @NamedQuery(name = "Provnacional.findByIdPROVNACIONAL", query = "SELECT p FROM Provnacional p WHERE p.idPROVNACIONAL = :idPROVNACIONAL"),
    @NamedQuery(name = "Provnacional.findByTipooperac", query = "SELECT p FROM Provnacional p WHERE p.tipooperac = :tipooperac"),
    @NamedQuery(name = "Provnacional.findByRFCProv", query = "SELECT p FROM Provnacional p WHERE p.rFCProv = :rFCProv"),
    @NamedQuery(name = "Provnacional.findByValactpagtas15o16IVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagtas15o16IVA = :valactpagtas15o16IVA"),
    @NamedQuery(name = "Provnacional.findByValactpagtas15IVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagtas15IVA = :valactpagtas15IVA"),
    @NamedQuery(name = "Provnacional.findByMonIVApagnoacrtas15o16", query = "SELECT p FROM Provnacional p WHERE p.monIVApagnoacrtas15o16 = :monIVApagnoacrtas15o16"),
    @NamedQuery(name = "Provnacional.findByValactpagtas10u11IVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagtas10u11IVA = :valactpagtas10u11IVA"),
    @NamedQuery(name = "Provnacional.findByValactpagtas10IVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagtas10IVA = :valactpagtas10IVA"),
    @NamedQuery(name = "Provnacional.findByMonIVApagnoacrtas10u11", query = "SELECT p FROM Provnacional p WHERE p.monIVApagnoacrtas10u11 = :monIVApagnoacrtas10u11"),
    @NamedQuery(name = "Provnacional.findByValactpagtas0IVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagtas0IVA = :valactpagtas0IVA"),
    @NamedQuery(name = "Provnacional.findByValactpagnoIVA", query = "SELECT p FROM Provnacional p WHERE p.valactpagnoIVA = :valactpagnoIVA"),
    @NamedQuery(name = "Provnacional.findByIVAretcont", query = "SELECT p FROM Provnacional p WHERE p.iVAretcont = :iVAretcont"),
    @NamedQuery(name = "Provnacional.findByIVAdevdescybonifcomp", query = "SELECT p FROM Provnacional p WHERE p.iVAdevdescybonifcomp = :iVAdevdescybonifcomp"),
    @NamedQuery(name = "Provnacional.findByDPIVAidDPIVA", query = "SELECT p FROM Provnacional p WHERE p.dPIVAidDPIVA = :dPIVAidDPIVA")})
public class Provnacional implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPROVNACIONAL")
    private Integer idPROVNACIONAL;
    @Column(name = "tipooperac")
    private String tipooperac;
    @Column(name = "RFCProv")
    private String rFCProv;
    @Column(name = "valactpagtas15o16IVA")
    private Integer valactpagtas15o16IVA;
    @Column(name = "valactpagtas15IVA")
    private Integer valactpagtas15IVA;
    @Column(name = "monIVApagnoacrtas15o16")
    private Integer monIVApagnoacrtas15o16;
    @Column(name = "valactpagtas10u11IVA")
    private Integer valactpagtas10u11IVA;
    @Column(name = "valactpagtas10IVA")
    private Integer valactpagtas10IVA;
    @Column(name = "monIVApagnoacrtas10u11")
    private Integer monIVApagnoacrtas10u11;
    @Column(name = "valactpagtas0IVA")
    private Integer valactpagtas0IVA;
    @Column(name = "valactpagnoIVA")
    private Integer valactpagnoIVA;
    @Column(name = "IVAretcont")
    private Integer iVAretcont;
    @Column(name = "IVAdevdescybonifcomp")
    private Integer iVAdevdescybonifcomp;
    @Basic(optional = false)
    @Column(name = "DPIVA_idDPIVA")
    private int dPIVAidDPIVA;

    public Provnacional() {
    }

    public Provnacional(Integer idPROVNACIONAL) {
        this.idPROVNACIONAL = idPROVNACIONAL;
    }

    public Provnacional(Integer idPROVNACIONAL, int dPIVAidDPIVA) {
        this.idPROVNACIONAL = idPROVNACIONAL;
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    public Integer getIdPROVNACIONAL() {
        return idPROVNACIONAL;
    }

    public void setIdPROVNACIONAL(Integer idPROVNACIONAL) {
        this.idPROVNACIONAL = idPROVNACIONAL;
    }

    public String getTipooperac() {
        return tipooperac;
    }

    public void setTipooperac(String tipooperac) {
        this.tipooperac = tipooperac;
    }

    public String getRFCProv() {
        return rFCProv;
    }

    public void setRFCProv(String rFCProv) {
        this.rFCProv = rFCProv;
    }

    public Integer getValactpagtas15o16IVA() {
        return valactpagtas15o16IVA;
    }

    public void setValactpagtas15o16IVA(Integer valactpagtas15o16IVA) {
        this.valactpagtas15o16IVA = valactpagtas15o16IVA;
    }

    public Integer getValactpagtas15IVA() {
        return valactpagtas15IVA;
    }

    public void setValactpagtas15IVA(Integer valactpagtas15IVA) {
        this.valactpagtas15IVA = valactpagtas15IVA;
    }

    public Integer getMonIVApagnoacrtas15o16() {
        return monIVApagnoacrtas15o16;
    }

    public void setMonIVApagnoacrtas15o16(Integer monIVApagnoacrtas15o16) {
        this.monIVApagnoacrtas15o16 = monIVApagnoacrtas15o16;
    }

    public Integer getValactpagtas10u11IVA() {
        return valactpagtas10u11IVA;
    }

    public void setValactpagtas10u11IVA(Integer valactpagtas10u11IVA) {
        this.valactpagtas10u11IVA = valactpagtas10u11IVA;
    }

    public Integer getValactpagtas10IVA() {
        return valactpagtas10IVA;
    }

    public void setValactpagtas10IVA(Integer valactpagtas10IVA) {
        this.valactpagtas10IVA = valactpagtas10IVA;
    }

    public Integer getMonIVApagnoacrtas10u11() {
        return monIVApagnoacrtas10u11;
    }

    public void setMonIVApagnoacrtas10u11(Integer monIVApagnoacrtas10u11) {
        this.monIVApagnoacrtas10u11 = monIVApagnoacrtas10u11;
    }

    public Integer getValactpagtas0IVA() {
        return valactpagtas0IVA;
    }

    public void setValactpagtas0IVA(Integer valactpagtas0IVA) {
        this.valactpagtas0IVA = valactpagtas0IVA;
    }

    public Integer getValactpagnoIVA() {
        return valactpagnoIVA;
    }

    public void setValactpagnoIVA(Integer valactpagnoIVA) {
        this.valactpagnoIVA = valactpagnoIVA;
    }

    public Integer getIVAretcont() {
        return iVAretcont;
    }

    public void setIVAretcont(Integer iVAretcont) {
        this.iVAretcont = iVAretcont;
    }

    public Integer getIVAdevdescybonifcomp() {
        return iVAdevdescybonifcomp;
    }

    public void setIVAdevdescybonifcomp(Integer iVAdevdescybonifcomp) {
        this.iVAdevdescybonifcomp = iVAdevdescybonifcomp;
    }

    public int getDPIVAidDPIVA() {
        return dPIVAidDPIVA;
    }

    public void setDPIVAidDPIVA(int dPIVAidDPIVA) {
        this.dPIVAidDPIVA = dPIVAidDPIVA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPROVNACIONAL != null ? idPROVNACIONAL.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provnacional)) {
            return false;
        }
        Provnacional other = (Provnacional) object;
        if ((this.idPROVNACIONAL == null && other.idPROVNACIONAL != null) || (this.idPROVNACIONAL != null && !this.idPROVNACIONAL.equals(other.idPROVNACIONAL))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Provnacional[ idPROVNACIONAL=" + idPROVNACIONAL + " ]";
    }
    
}
