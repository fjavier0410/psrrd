/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "runidadorg")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Runidadorg.findAll", query = "SELECT r FROM Runidadorg r"),
    @NamedQuery(name = "Runidadorg.findByIdRUNIDADORG", query = "SELECT r FROM Runidadorg r WHERE r.idRUNIDADORG = :idRUNIDADORG"),
    @NamedQuery(name = "Runidadorg.findByRunidad", query = "SELECT r FROM Runidadorg r WHERE r.runidad = :runidad"),
    @NamedQuery(name = "Runidadorg.findByREMPLEADODEidREMPLEADODE", query = "SELECT r FROM Runidadorg r WHERE r.rEMPLEADODEidREMPLEADODE = :rEMPLEADODEidREMPLEADODE")})
public class Runidadorg implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRUNIDADORG")
    private Integer idRUNIDADORG;
    @Column(name = "runidad")
    private String runidad;
    @Basic(optional = false)
    @Column(name = "REMPLEADODE_idREMPLEADODE")
    private int rEMPLEADODEidREMPLEADODE;

    public Runidadorg() {
    }

    public Runidadorg(Integer idRUNIDADORG) {
        this.idRUNIDADORG = idRUNIDADORG;
    }

    public Runidadorg(Integer idRUNIDADORG, int rEMPLEADODEidREMPLEADODE) {
        this.idRUNIDADORG = idRUNIDADORG;
        this.rEMPLEADODEidREMPLEADODE = rEMPLEADODEidREMPLEADODE;
    }

    public Integer getIdRUNIDADORG() {
        return idRUNIDADORG;
    }

    public void setIdRUNIDADORG(Integer idRUNIDADORG) {
        this.idRUNIDADORG = idRUNIDADORG;
    }

    public String getRunidad() {
        return runidad;
    }

    public void setRunidad(String runidad) {
        this.runidad = runidad;
    }

    public int getREMPLEADODEidREMPLEADODE() {
        return rEMPLEADODEidREMPLEADODE;
    }

    public void setREMPLEADODEidREMPLEADODE(int rEMPLEADODEidREMPLEADODE) {
        this.rEMPLEADODEidREMPLEADODE = rEMPLEADODEidREMPLEADODE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRUNIDADORG != null ? idRUNIDADORG.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Runidadorg)) {
            return false;
        }
        Runidadorg other = (Runidadorg) object;
        if ((this.idRUNIDADORG == null && other.idRUNIDADORG != null) || (this.idRUNIDADORG != null && !this.idRUNIDADORG.equals(other.idRUNIDADORG))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Runidadorg[ idRUNIDADORG=" + idRUNIDADORG + " ]";
    }
    
}
