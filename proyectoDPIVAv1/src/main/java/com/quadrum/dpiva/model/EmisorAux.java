package com.quadrum.dpiva.model;



import org.springframework.web.multipart.MultipartFile;

import lombok.Data;


@Data
public class EmisorAux {
	private String erfc;
    private String ecurp;
    private String eapellpat;
    private String eapellmat;
    private String enombre;
    private String edenrazsoc;
    private String ecorreoe;
    private String enumtel;
    private MultipartFile fileCer; 
    private MultipartFile fileKey; 
    private String passwordKey;
    
    
}
