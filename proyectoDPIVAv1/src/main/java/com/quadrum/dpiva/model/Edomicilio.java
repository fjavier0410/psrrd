/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quadrum.dpiva.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Denova
 */
@Entity
@Table(name = "edomicilio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Edomicilio.findAll", query = "SELECT e FROM Edomicilio e"),
    @NamedQuery(name = "Edomicilio.findByIdEDOMICILIO", query = "SELECT e FROM Edomicilio e WHERE e.idEDOMICILIO = :idEDOMICILIO"),
    @NamedQuery(name = "Edomicilio.findByEcalle", query = "SELECT e FROM Edomicilio e WHERE e.ecalle = :ecalle"),
    @NamedQuery(name = "Edomicilio.findByEnumext", query = "SELECT e FROM Edomicilio e WHERE e.enumext = :enumext"),
    @NamedQuery(name = "Edomicilio.findByEnumint", query = "SELECT e FROM Edomicilio e WHERE e.enumint = :enumint"),
    @NamedQuery(name = "Edomicilio.findByEcolonia", query = "SELECT e FROM Edomicilio e WHERE e.ecolonia = :ecolonia"),
    @NamedQuery(name = "Edomicilio.findByElocalidad", query = "SELECT e FROM Edomicilio e WHERE e.elocalidad = :elocalidad"),
    @NamedQuery(name = "Edomicilio.findByEref", query = "SELECT e FROM Edomicilio e WHERE e.eref = :eref"),
    @NamedQuery(name = "Edomicilio.findByEnumdel", query = "SELECT e FROM Edomicilio e WHERE e.enumdel = :enumdel"),
    @NamedQuery(name = "Edomicilio.findByEentidadf", query = "SELECT e FROM Edomicilio e WHERE e.eentidadf = :eentidadf"),
    @NamedQuery(name = "Edomicilio.findByEmundel", query = "SELECT e FROM Edomicilio e WHERE e.emundel = :emundel"),
    @NamedQuery(name = "Edomicilio.findByEpais", query = "SELECT e FROM Edomicilio e WHERE e.epais = :epais"),
    @NamedQuery(name = "Edomicilio.findByEcp", query = "SELECT e FROM Edomicilio e WHERE e.ecp = :ecp")})
public class Edomicilio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idEDOMICILIO")
    private Integer idEDOMICILIO;
    @Column(name = "ecalle")
    private String ecalle;
    @Column(name = "enumext")
    private String enumext;
    @Column(name = "enumint")
    private String enumint;
    @Column(name = "ecolonia")
    private String ecolonia;
    @Column(name = "elocalidad")
    private String elocalidad;
    @Column(name = "eref")
    private String eref;
    @Column(name = "enumdel")
    private String enumdel;
    @Column(name = "eentidadf")
    private String eentidadf;
    @Column(name = "emundel")
    private String emundel;
    @Column(name = "epais")
    private String epais;
    @Column(name = "ecp")
    private String ecp;

    public Edomicilio() {
    }

    public Edomicilio(Integer idEDOMICILIO) {
        this.idEDOMICILIO = idEDOMICILIO;
    }

    public Integer getIdEDOMICILIO() {
        return idEDOMICILIO;
    }

    public void setIdEDOMICILIO(Integer idEDOMICILIO) {
        this.idEDOMICILIO = idEDOMICILIO;
    }

    public String getEcalle() {
        return ecalle;
    }

    public void setEcalle(String ecalle) {
        this.ecalle = ecalle;
    }

    public String getEnumext() {
        return enumext;
    }

    public void setEnumext(String enumext) {
        this.enumext = enumext;
    }

    public String getEnumint() {
        return enumint;
    }

    public void setEnumint(String enumint) {
        this.enumint = enumint;
    }

    public String getEcolonia() {
        return ecolonia;
    }

    public void setEcolonia(String ecolonia) {
        this.ecolonia = ecolonia;
    }

    public String getElocalidad() {
        return elocalidad;
    }

    public void setElocalidad(String elocalidad) {
        this.elocalidad = elocalidad;
    }

    public String getEref() {
        return eref;
    }

    public void setEref(String eref) {
        this.eref = eref;
    }

    public String getEnumdel() {
        return enumdel;
    }

    public void setEnumdel(String enumdel) {
        this.enumdel = enumdel;
    }

    public String getEentidadf() {
        return eentidadf;
    }

    public void setEentidadf(String eentidadf) {
        this.eentidadf = eentidadf;
    }

    public String getEmundel() {
        return emundel;
    }

    public void setEmundel(String emundel) {
        this.emundel = emundel;
    }

    public String getEpais() {
        return epais;
    }

    public void setEpais(String epais) {
        this.epais = epais;
    }

    public String getEcp() {
        return ecp;
    }

    public void setEcp(String ecp) {
        this.ecp = ecp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEDOMICILIO != null ? idEDOMICILIO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Edomicilio)) {
            return false;
        }
        Edomicilio other = (Edomicilio) object;
        if ((this.idEDOMICILIO == null && other.idEDOMICILIO != null) || (this.idEDOMICILIO != null && !this.idEDOMICILIO.equals(other.idEDOMICILIO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demodpiva.Edomicilio[ idEDOMICILIO=" + idEDOMICILIO + " ]";
    }
    
}
