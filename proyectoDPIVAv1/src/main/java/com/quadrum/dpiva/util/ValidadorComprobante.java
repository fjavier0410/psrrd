package com.quadrum.dpiva.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.extern.log4j.Log4j;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.quadrum.dpiva.wrapper.DatosIdentifcacionWrapper;

@Log4j
public class ValidadorComprobante implements Validator{
	private static final int A�O_ACTUAL = 
			Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date()));	
	
	

	@Override
	public boolean supports(Class<?> arg0) {
		System.out.println("Holaa");
		return DatosIdentifcacionWrapper.class.equals(arg0); // clase del bean al que da soporte este validador
	}

	@Override
	public void validate(Object target, Errors errors) {
		DatosIdentifcacionWrapper datosIdentificacion = (DatosIdentifcacionWrapper) target;
		
		
		// RFC es obligatoria
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rfc", "rfc.required", 
				"La matr�cula es obligatoria");
		
		// debe tener un formato correcto del tipo: 1111-BBB o B-2222-MM
		validarFormatoMatricula(datosIdentificacion.getRfc(), errors); // valida la matricula por expresi�n regular
		
		// curp es obligatoria
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "curp", "curp.required", 
			"El modelo es obligatorio");
		
		// el a�o debe ser v�lido no puede ser mayor que el actual
		if ( datosIdentificacion.getEjercicio() < 2015 || datosIdentificacion.getEjercicio()> A�O_ACTUAL ) {
			errors.rejectValue("ejercicio", "field.ejercicio.invalid", "El ejercicio es incorrecto");
		}	
		
		// si no hay errores relacionados con el campo a�o
		if ( ! errors.hasFieldErrors("anho")) {
		
			// para los coches de a�o distinto al actual, validamos que no tengan m�s de 100.000 km
			if ( datosIdentificacion.getEjercicio()< A�O_ACTUAL ) {
				
				
				
				
			
			}		
		}		
	}
	
	private void validarFormatoMatricula (String matricula, Errors errors) {
		// valida la matr�cula por expresi�n regular, si hay error lo a�ade a errors
	}

}
