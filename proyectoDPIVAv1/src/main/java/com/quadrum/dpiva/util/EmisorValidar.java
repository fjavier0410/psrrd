package com.quadrum.dpiva.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.cert.CertificateException;










import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.quadrum.dpiva.controller.ComprobanteController;
import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.EmisorAux;

@Slf4j
public class EmisorValidar implements Validator {
	
	
	private  Validaciones objValidaciones=null; 
	private ValidacionesCertificado validacionesCertificado;
	private ValidacionesCertificado validaCertificado; 
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
	
		 return Emisor.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object object, Errors errors) {
		EmisorAux objEmisor=(EmisorAux) object; 
		objValidaciones=new Validaciones(); 
		
		ValidationUtils.rejectIfEmpty(errors, "erfc", "required.erfc",  "El RFC es obligatorio");
		ValidationUtils.rejectIfEmpty(errors, "passwordKey", "required.passwordKey",  "La contrase�a es obligatoria");
		ValidationUtils.rejectIfEmpty(errors, "fileCer", "required.filekey",  "Ingrese un archivo .key");
		
		
		if(objEmisor.getFileCer()==null)
			ValidationUtils.rejectIfEmpty(errors, "fileCer", "required.fileCer",  "Ingrese un archivo .cer");
		

		if(objEmisor.getFileKey()==null)
			ValidationUtils.rejectIfEmpty(errors, "fileCer", "required.filekey",  "Ingrese un archivo .key");
		
		if (objEmisor.getErfc() != null && !objEmisor.getErfc().isEmpty()) {
			if(!objValidaciones.validaRFC(objEmisor.getErfc()))
			{
				errors.rejectValue("erfc","erfc.incorrect", "El RFC no tiene un patr�n valido");
			}
		}
		
		if(objEmisor.getEcurp()!=null && !objEmisor.getEcurp().isEmpty())
		{
			if(!objValidaciones.validarCURP(objEmisor.getEcurp()))
			{
				errors.rejectValue("ecurp", "ecurp.incorrect","La CURP no tiene un patron valido");
			}
		}
		
		
		if(objEmisor.getEcorreoe()!=null && !objEmisor.getEcorreoe().isEmpty())
		{
			if(!objValidaciones.validarCURP(objEmisor.getEcurp()))
			{
				errors.rejectValue("ecorreoe", "ecorreoe.incorrect","El correo no es valido");
			}
		}
		
		
		if (objEmisor.getFileCer() != null && objEmisor.getFileKey() != null && objEmisor.getPasswordKey()!=null && !objEmisor.getPasswordKey().isEmpty() && objEmisor.getErfc()!=null && !objEmisor.getErfc().isEmpty()) {
			
	     try {
			convierteInpuToFile(objEmisor.getFileCer().getBytes());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	       // MultipartFile multipart = objEmisor.getFileCer().getMultipartFile();
	        try {
				validacionesCertificado = new ValidacionesCertificado(objEmisor.getFileCer().getInputStream(), objEmisor.getFileKey().getInputStream(), objEmisor.getPasswordKey());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!objEmisor.getFileCer().getName().endsWith(".cer"))
				ValidationUtils.rejectIfEmpty(errors, "fileCer", "required.fileKey",  "Ingrese un archivo .cer");
			
			if(!objEmisor.getFileKey().getName().endsWith(".key"))
				ValidationUtils.rejectIfEmpty(errors, "fileKey", "required.fileKey",  "Ingrese un archivo .key");
			
				
			if (!validacionesCertificado.validaCorrespondencias()) {
			
				errors.rejectValue("passwordKey", "passwordKey.incorrect","Los certificados y la llave privada que se han cargado no corresponden, favor de verificarlos");
				
			}
			else 
			{
				
				ByteArrayOutputStream inputStreamOutCer = new ByteArrayOutputStream();
				try {
					IOUtils.copy(objEmisor.getFileCer().getInputStream(), inputStreamOutCer);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				validaCertificado = new ValidacionesCertificado(new ByteArrayInputStream(inputStreamOutCer.toByteArray()));
				
				if (!validaCertificado.validaCertificado(objEmisor.getErfc())) 
							errors.rejectValue("passwordKey", "passwordKey.incorrect","Los archivos cargados no corresponden al Emisor");
			
				if (validaCertificado.validaFechaCertificado().equals("caduco"))
					errors.rejectValue("passwordKey", "passwordKey.incorrect","Archivo con contenido no valido el certificado ha caducado, requiere uno vigente");
				if(!validaCertificado.validaCertificado())
				{
					errors.rejectValue("passwordKey", "passwordKey.incorrect","El archivo cargado es Fiel requiere un archivo .cer valido");
				}
				
				
				
				try {
					if(!validaCertificado.ValidateCertificate(validaCertificado.importCertificate(new ByteArrayInputStream(inputStreamOutCer.toByteArray()))))
					{
						errors.rejectValue("passwordKey", "passwordKey.incorrect","EL certificado cargado no es expedido por el SAT, requiere un archivo .cer valido");
						
					}
				} catch (CertificateException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	
			
			//if(validaCertificado.validaFechaCertificado().contains("caduco"))
			//	errors.rejectValue("passwordKey", "passwordKey.incorrect","El archivo cargado ha caducado, requiere uno vigente");
		/*	if(!validaCertificado.validaCertificado())
				errors.rejectValue("passwordKey", "passwordKey.incorrect","El archivo cargado es FIEL, equiere un archivo .cer valido");
			try {
				IOUtils.copy(bisCer, bisCerOut);
				if(!ValidacionesCertificado.ValidateCertificate(ValidacionesCertificado.importCertificate(new ByteArrayInputStream(bisCerOut.toByteArray()))))
					errors.rejectValue("passwordKey", "passwordKey.incorrect","Certificado no expedido por el SAT");
			} catch (CertificateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
		}
		
	}
	
	private InputStream convierteAInputstream(byte[] content)
	{
	        int size = content.length;
	        InputStream is = null;
	        byte[] b = new byte[size];
	        try {
	            is = new ByteArrayInputStream(content);
	            is.read(b);
	            System.out.println("Data Recovered: "+new String(b));
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try{
	                if(is != null) is.close();
	            } catch (Exception ex){
	                 
	            }
	        }
	        return is;
	}
	
	
private void convierteInpuToFile(byte[] bFile)
{
	
	FileOutputStream fos;
	try {
		fos = new FileOutputStream("C:\\quadrum\\certificado.cer");
		fos.write(bFile);
		fos.close();
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}

	
	
	
}
