package com.quadrum.dpiva.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validaciones {

	
	public boolean validarCURP(String curp) {
		String regex = "[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}"
				+ "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])"
				+ "[HM]{1}"
				+ "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)"
				+ "[B-DF-HJ-NP-TV-Z]{3}" + "[0-9A-Z]{1}[0-9]{1}$";
		Pattern patron = Pattern.compile(regex);
		if (!patron.matcher(curp).matches()) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean validarCorreo(String correo)
	{
		  Pattern pattern = Pattern
	                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	
	        Matcher mather = pattern.matcher(correo);
	 
	        if (mather.find() == true) 
	           return true; 
	         else 
	        	 return false; 
	        
	}
	
	
	public boolean validaRFC(String rfc)
	{
		String patronRfc = "^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]{3}$";
		Matcher matcher; 
		Pattern pat = Pattern.compile(patronRfc);
		matcher = pat.matcher(rfc);
		if (!matcher.matches()) {
			return false; 
		}
		return true; 
		
	}
	
}
