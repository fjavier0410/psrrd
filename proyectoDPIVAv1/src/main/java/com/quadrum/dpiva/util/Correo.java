package com.quadrum.dpiva.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class Correo {
	
    private static final String CORREO="nomifast@quadrum.mx";
    private static final String HOST="quadrum.mx"; 
    private static final String PASSWORD="Quadrum201"; 
    private static final String PORT="587";
	Cifrado objDesencripcion= new Cifrado(); 
    public void enviaEmail(String correo, String asunto, String password) throws MessagingException {
        
        try{
        	
            String someHtmlMessage="<html>"
            		+ "<body>"
            		+ "<div style='margin:0 auto 0 auto; width:490px; text-aling='center''>"
            		+ "<div id='header'> <h2>Recuperacion de contrase�a </h2> </div>"
            		+ "<div  style='font-family:Arial;font-size:12px;padding:40px;line-height:22px; background-color:#000000; height:300px;color:#FFFFFF;'> "
            		+ "Tu contrase�a ha sido reestablecida:"+generaPassword()
            		+ "<br>Por favor ingresa  a la brevedad con tu usuario actual y la nueva contrase�a. Al ingresar, podr�s modificar tu contrase�a en la secci�n �Mi Cuenta� en el Men� superior derecho de la p�gina."
            		+ "Gracias.  "
            		+ ""
            		+ "</div>"
            		+ "<div  style='padding:40px;text-align:justify;font-family:Arial;font-size:8px;background:#58595b;color:#ffffff;line-height:1.5'>"
            		+ "Est�s recibiendo este correo porque est�s suscrito a trav�s de nuestro sitio web."
            		+ " </div>"
            		+ "</div>"
            		+ ""
            		+ " </body>"
            		+ ""
            		+ " </html>"; 
            Properties props = new Properties();
            props.setProperty("mail.host", HOST);
            props.setProperty("mail.smtp.port", PORT);
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "false");
            props.put("mail.smtp.socketFactory.port", PORT);
            props.put("mail.smtp.socketFactory.fallback", "false");
            

            Authenticator auth = new SMTPAuthenticator(CORREO,PASSWORD); 
            Session session = Session.getInstance(props, auth);

            MimeMessage msg = new MimeMessage(session);
            msg.setContent(someHtmlMessage, "text/html; charset=utf-8");
            /* System.out.println("Message: "+message);
            System.out.println("Subject: "+subject);
            System.out.println("From: "+from);
            System.out.println("To: "+to);
            System.out.println("Msg: "+msg); */
            
            
            msg.setSubject(asunto);
            msg.setFrom(new InternetAddress(CORREO));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(correo));
            Transport.send(msg);
        }catch(MessagingException m){
            
           
    }
    }
    private class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
    
    public String generaPassword() {
        String password = "";

        int numChar = aleatorio(8, 15); //genera la contraseña de 8 a 15 caracteres
        while (numChar > 0) {
            int eleccion = aleatorio(1, 4);
            if (eleccion == 1)
                password += (char)aleatorio(97, 122); //genera una letra minúscula
            if (eleccion == 2)
                password += (char)aleatorio(65, 90); //genera una letra mayúscula
            if (eleccion == 3)
                password += (char)aleatorio(48, 57); //genera un digito
            if (eleccion == 4)
                password += (char)aleatorio(33, 35); //genera un caracter especial
            numChar--;
        }
        return password;
    }
    
    public int aleatorio(int a, int b) {
        return (int)Math.floor(Math.random() * (b - a + 1) + a);
    }
	
    public static void main(String args[])
    {
    	Correo objCorreo=new Correo(); 
    	 System.out.println(objCorreo.generaPassword()); 
    }
	
}
