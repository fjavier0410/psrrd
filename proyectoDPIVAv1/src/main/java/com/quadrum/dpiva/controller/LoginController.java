package com.quadrum.dpiva.controller;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.Usuario;
import com.quadrum.dpiva.service.EmisorService;
import com.quadrum.dpiva.service.UsuarioService; 
import com.quadrum.dpiva.util.Correo;
import com.quadrum.dpiva.util.Cifrado;

@Controller
public class LoginController {
	
	@Autowired
	private EmisorService emisorService;
	@Autowired
	private UsuarioService usuarioServices; 
	
	
	
	@RequestMapping("/login")
	public String doLogin(){
		
		
		return "login";
	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
                @RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout,
                HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error",
                           getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView recuperaPassword(@RequestParam("erfc") String rfcEmisor) {
		Correo objCorreo = null;
		Emisor objEmisor = emisorService.buscaPorRfcEmisor(rfcEmisor);
		Usuario objUsuario = null;
		Cifrado objDesencripcion = new Cifrado();
		if (objEmisor != null) {
			if (objEmisor.getEcorreoe() != null) {
				objCorreo = new Correo();
				try {
					objUsuario = new Usuario();
					objUsuario = usuarioServices.buscaPorId(objEmisor
							.getIdusuario().getIdusuario());
					if (objUsuario != null) {
						String password = objDesencripcion.encrypt(objCorreo.generaPassword());
						objUsuario.setPass(password);
						objCorreo.enviaEmail(objEmisor.getEcorreoe(),
								"Recuperacion de contraseņa", password);
						usuarioServices.editaUsuario(objUsuario);

					}
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		return new ModelAndView("login");
	}

	@RequestMapping(value="/recuperacionpass", method = RequestMethod.GET)
    public ModelAndView recuperaPasscord()
    {
		ModelAndView model=new ModelAndView();
		model.setViewName("recupera_pass");
		model.addObject("recuperarPass",new String());
        return model; 
    }

	
	private String getErrorMessage(HttpServletRequest request, String key){

		Exception exception =
                   (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		}else if(exception instanceof LockedException) {
			error = exception.getMessage();
		}else{
			error = "Invalid username and password!";
		}

		return error;
	}
	
}
