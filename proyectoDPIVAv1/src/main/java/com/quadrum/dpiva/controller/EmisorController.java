package com.quadrum.dpiva.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.quadrum.dpiva.model.Certificados;
import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.EmisorAux;
import com.quadrum.dpiva.model.Usuario;
import com.quadrum.dpiva.service.EmisorService;
import com.quadrum.dpiva.service.UsuarioService;
import com.quadrum.dpiva.util.Cifrado;
import com.quadrum.dpiva.util.EmisorValidar;
import com.quadrum.dpiva.util.ValidacionesCertificado;

@Slf4j
@Controller
public class EmisorController {
	
	@Autowired
	private EmisorService emisorService;
	@Autowired
	private UsuarioService usuarioService;
	private EmisorValidar emisorValidar; 
	private static final Logger logger = Logger.getLogger(EmisorController.class);
	
	
	public EmisorController()
	{
		emisorValidar=new EmisorValidar(); 
	}
	
	
	@RequestMapping("/")
	public String setupForm(Map<String,Object> map){
		Emisor emisor = new Emisor();
		map.put("emisor", emisor);
		map.put("emisorList", emisorService.traerTodosEmisores());
		
		return "page";
	}
	
	@RequestMapping(value="/emisor.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Emisor emisor,BindingResult result,@RequestParam String action,Map<String,Object> map){
		Emisor emisorResult = new Emisor();
		switch(action.toLowerCase()){
		case "add":
			emisorService.agregar(emisor);
			emisorResult = emisor;
			break;
		case "edit":
			emisorService.actualizar(emisor);
			emisorResult = emisor;
			break;
		case "delete":
			emisorService.eliminar(emisor.getIdEMISOR());
			emisorResult = emisor;
			break;
		case "seach":
			Emisor searchEmisor = emisorService.traerEmisor(emisor.getIdEMISOR());
			emisorResult = searchEmisor!=null ? searchEmisor : new Emisor();
			break;
		
		}
		map.put("emisor", emisorResult);
		map.put("emisorList", emisorService.traerTodosEmisores());
		
		return "page";
	}
	
	
	 @RequestMapping(value="/emisor/alta", method = RequestMethod.GET)
	    public ModelAndView form()
	    {
	        ModelAndView mav=new ModelAndView(); 
	        mav.setViewName("/emisor/alta_emisor");
	        mav.addObject("emisorRegister",new EmisorAux()); 
	        return mav; 
	    }
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView altaContribuyente(@ModelAttribute("emisorRegister") EmisorAux objEmisor, BindingResult bindingResult, SessionStatus status){
		
		emisorValidar.validate(objEmisor, bindingResult);
		ModelAndView mav=null; 
		if(bindingResult.hasErrors())
		{
			 mav=new ModelAndView(); 
             mav.setViewName("/emisor/alta_emisor");
             mav.addObject("emisorRegister",new EmisorAux()); 
		}
		else 
		{
		    mav=new ModelAndView(); 
		   emisorService.agregar(convierteEmisor(objEmisor));
			mav.setViewName("/emisor/emisor"); 
		}
		return mav; 
		
	}
	
	

	 
	
	
	private Emisor convierteEmisor(EmisorAux objEmisorAux)
	{
		
		Emisor objEmisorBase=new Emisor(); 
	try
	{
		if(objEmisorAux.getEapellmat()!=null)
			objEmisorBase.setEapellmat(Cifrado.encrypt(objEmisorAux.getEapellmat()));
		if(objEmisorAux.getEapellpat()!=null)
			objEmisorBase.setEapellpat(Cifrado.encrypt(objEmisorAux.getEapellpat()));
		if(objEmisorAux.getEcorreoe()!=null)
			objEmisorBase.setEcorreoe(Cifrado.encrypt(objEmisorBase.getEcorreoe()));
		if(objEmisorAux.getEcurp()!=null)
			objEmisorBase.setEcurp(Cifrado.encrypt(objEmisorBase.getEcurp()));
		if(objEmisorAux.getEdenrazsoc()!=null)
			objEmisorBase.setEdenrazsoc(Cifrado.encrypt(objEmisorBase.getEdenrazsoc()));
		if(objEmisorAux.getEnombre()!=null)
			objEmisorBase.setEnombre(Cifrado.encrypt(objEmisorAux.getEdenrazsoc()));
		if(objEmisorAux.getEnumtel()!=null)
			objEmisorBase.setEnumtel(Cifrado.encrypt(objEmisorAux.getEnumtel()));
		if(objEmisorAux.getErfc()!=null)
			objEmisorBase.setErfc(Cifrado.encrypt(objEmisorAux.getErfc()));
		if(objEmisorAux.getPasswordKey()!=null)
			objEmisorBase.setPasswordKey(Cifrado.encrypt(objEmisorAux.getPasswordKey()));
	}catch(Exception e)
	{
		e.printStackTrace();
	}
		if(objEmisorAux.getFileCer()!=null)
		{	try {
				objEmisorBase.setFileCer(objEmisorAux.getFileCer().getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(objEmisorAux.getFileKey()!=null)
		{	try {
				objEmisorBase.setFileKey(objEmisorAux.getFileKey().getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			Usuario objUsuario=usuarioService.buscaPorRFC(objEmisorAux.getErfc()); 
		if(objUsuario!=null)
		{
			objUsuario.setEstatus(1);
			objEmisorBase.setIdusuario(objUsuario);
			usuarioService.editaUsuario(objUsuario);
		}
		return objEmisorBase;
	}
	

}
