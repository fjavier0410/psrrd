package com.quadrum.dpiva.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.quadrum.dpiva.wrapper.DatosGeneralesWrapper;

@Log4j 
@Controller
public class DatosGenerales {
	@RequestMapping(value = "/datos", method = RequestMethod.POST)
	public String secondPage(Model model) {
		DatosGeneralesWrapper datosGenerales = new DatosGeneralesWrapper();
		
		model.addAttribute("datos",datosGenerales);
		
		
		
		System.out.println("EEEEEEEEEEEEEE aqui entro ");
		return "datos";
	}
	
//	@ModelAttribute("operacionesTercerosList")
//	 public Map<String,String> populateOperacionesTerceros() {
//	   
//	  //Data referencing for java skills list box
//	  Map<String,String> operaciones = new LinkedHashMap<String,String>();
//	  operaciones.put("conDatos", "La presenta con datos");
//	  operaciones.put("sinDatos", "La presenta sin operaciones");
//	   
//	  return operaciones;
//	 }
//	
	
	@ModelAttribute("tipoDeclaracionList")
	 public Map<String,String> populateTipoDeclaracion() {
	   
	  //Data referencing for java skills list box
	  Map<String,String> declaracion = new LinkedHashMap<String,String>();
	  declaracion.put("normal", "Normal");
	  declaracion.put("complementaria", "Complementaria");
	   
	  return declaracion;
	 }
	
	
	@ModelAttribute("tipoPeriodicidadList")
	 public Map<String,String> populateTipoPeriodicidad() {
	   
	  //Data referencing for java skills list box
	  Map<String,String> periodicidad = new LinkedHashMap<String,String>();
	  periodicidad.put("mensual", "Mensual");
	  periodicidad.put("bimestral", "Bimestral");
	  periodicidad.put("Trimestral", "Trimestral");
	  periodicidad.put("Semestral","Semestral");
	   
	  return periodicidad;
	 }
	
	
	
}
