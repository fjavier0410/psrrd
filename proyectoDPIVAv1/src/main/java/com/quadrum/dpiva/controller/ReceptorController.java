package com.quadrum.dpiva.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.Receptor;
import com.quadrum.dpiva.service.ReceptorService;

@Controller
public class ReceptorController {
	
	
	@Autowired
	private ReceptorService receptorService;
	
	@RequestMapping("/receptor")
	public String setupForm(Map<String,Object> map){
		Receptor receptor = new Receptor();
		map.put("receptor", receptor);
		map.put("receptorList", receptorService.traerTodosReceptores());		
		return "hello";
	}
	
	
	@RequestMapping(value="/receptor.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Receptor receptor,BindingResult result,@RequestParam String action,Map<String,Object> map){
		Receptor receptorResult = new Receptor();
		switch(action.toLowerCase()){
		case "add":
			receptorService.agregar(receptor);
			receptorResult = receptor;
			break;
		case "edit":
			receptorService.actualizar(receptor);
			receptorResult = receptor;
			break;
		case "delete":
			receptorService.eliminar(receptor.getIdRECEPTOR());
			receptorResult = receptor;
			break;
		case "search":
			Receptor searchReceptor = receptorService.traeReceptorPorRfc(receptor.getRrfc());
			receptorResult = searchReceptor!=null ? searchReceptor : new Receptor();
			break;
		
		}
		map.put("receptor", receptorResult);
		map.put("receptorList", receptorService.traerTodosReceptores());
		
		return "receptor";
	}
}
