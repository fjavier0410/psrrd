package com.quadrum.dpiva.controller;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.quadrum.dpiva.model.Usuario;
import com.quadrum.dpiva.service.UsuarioService;


@Log4j
public class LoginAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	 
	
	@Autowired
	private UsuarioService usuarioServices; 
	 
	  private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	  
	    @Override
	    public void onAuthenticationSuccess(HttpServletRequest request, 
	      HttpServletResponse response, Authentication authentication) throws IOException {
	        handle(request, response, authentication);
	        clearAuthenticationAttributes(request);
	    }
	 
	    protected void handle(HttpServletRequest request, 
	      HttpServletResponse response, Authentication authentication) throws IOException {
	    	   String rfc=authentication.getName();
		       Usuario objUsuario=usuarioServices.buscaPorRFC(rfc); 
		       
	    	String targetUrl = determineTargetUrl(authentication, objUsuario.getEstatus());
	    
	        if (response.isCommitted()) {
	         
	            return;
	        }
	 
	        redirectStrategy.sendRedirect(request, response, targetUrl);
	    }
	 
	    /** Builds the target URL according to the logic defined in the main class Javadoc. */
	    protected String determineTargetUrl(Authentication authentication, int estatus  ) {
	        boolean isUser = false;
	        boolean isAdmin = false;
	        boolean isUserRegistrado = false;
	        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
	        for (GrantedAuthority grantedAuthority : authorities) {
	            if (grantedAuthority.getAuthority().equals("User") && estatus==0) {
	            	//redirigir a registro
	                isUser = true;
	                break;
	            } else if (grantedAuthority.getAuthority().equals("User") && estatus==1) {
	                //redirigir a pagina normal
	            	isUserRegistrado = true;
	                break;
	            }
	        }
	 
	        if (isUser) {
	            return "/emisor/alta";
	        } else if (isUserRegistrado) {
	            return "/";
	        } else {
	            throw new IllegalStateException();
	        }
	    }
	 
	    protected void clearAuthenticationAttributes(HttpServletRequest request) {
	        HttpSession session = request.getSession(false);
	        if (session == null) {
	            return;
	        }
	        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	    }
	 
	    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
	        this.redirectStrategy = redirectStrategy;
	    }
	    protected RedirectStrategy getRedirectStrategy() {
	        return redirectStrategy;
	    }
	

}
