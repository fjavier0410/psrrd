package com.quadrum.dpiva.controller;

import javax.validation.Valid;







import lombok.extern.log4j.Log4j;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.quadrum.dpiva.util.ValidadorComprobante;
import com.quadrum.dpiva.wrapper.DatosIdentifcacionWrapper;

@Log4j
@Controller
public class FormularioController {
	
	@RequestMapping(value = "/formulario", method = RequestMethod.GET)
	public String secondPage(Model model) {
		DatosIdentifcacionWrapper datosIdentificacion = new DatosIdentifcacionWrapper();
		System.out.println("====> Hola mundo ");
		
		model.addAttribute("datosIdentificacion", datosIdentificacion);
		return "datosIdentificacion";
	}
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new ValidadorComprobante()); // registramos el validador
    }
	
	
	@RequestMapping(value = "/datosIdentificacion", method = RequestMethod.POST)
    public String manejarFormularioYaValidado(@Valid DatosIdentifcacionWrapper datosIdentificacion, 
    		BindingResult result) {
		
		
		
		// si hay errores volvemos a la vista del formulario
		//if ( result.hasErrors() ) {
		//	System.out.println("hhhhhhhhh");
		//	log.info("Holaa");
		//	System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmmmmm");
		//	return "datos";
		//}
		
		
		System.out.println("ddddd");
		// si no hay errores, manejamos los datos validados...
		
		return "datos";
		
	}
	
	
	@ModelAttribute("datosIdentificacion")
	public DatosIdentifcacionWrapper populateForm() {
	     return new DatosIdentifcacionWrapper(); // creamos el bean para que se pueda popular
	}

}
