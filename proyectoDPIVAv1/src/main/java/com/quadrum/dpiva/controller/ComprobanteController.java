package com.quadrum.dpiva.controller;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.quadrum.dpiva.jaxb.CPais;
import com.quadrum.dpiva.jaxb.DoctoDigital;
import com.quadrum.dpiva.jaxb.ObjectFactory;
import com.quadrum.dpiva.jaxb.DoctoDigital.TipoDoctoDigital;
import com.quadrum.dpiva.model.Documentodigital;
import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.model.Receptor;
import com.quadrum.dpiva.model.Totales;
import com.quadrum.dpiva.wrapper.DatosGeneralesWrapper;

@Controller
public class ComprobanteController {
	private static final Logger logger = Logger.getLogger(ComprobanteController.class);

	
	
	@RequestMapping("/comprobante")
	public String setupForm(Map<String,Object> map,Model model){
DatosGeneralesWrapper datosGenerales = new DatosGeneralesWrapper();
Documentodigital documento = new Documentodigital();
		
		model.addAttribute("comprobante",datosGenerales);
		//map.put("comprobante", receptor);
		
		logger.info("Holaaaaaaaaaaaaaaaaaaaaa");
		logger.error("kkkkkkkkkkkkkkkkkk");
		System.out.println("EEEEEEEEEEEEEE11111111111111111111");
		
		return "comprobante";
	}
	
	@ModelAttribute("operacionesTercerosList")
	 public Map<String,String> populateOperacionesTerceros() {
	   
	  //Data referencing for java skills list box
	  Map<String,String> operaciones = new LinkedHashMap<String,String>();
	  operaciones.put("conDatos", "La presenta con datos");
	  operaciones.put("sinDatos", "La presenta sin operaciones");
	   
	  return operaciones;
	 }
	
	
	@ModelAttribute("tipoDeclaracionList")
	 public Map<String,String> populateTipoDeclaracion() {
	   
	  //Data referencing for java skills list box
	  Map<String,String> declaracion = new LinkedHashMap<String,String>();
	  declaracion.put("normal", "Normal");
	  declaracion.put("complementaria", "Complementaria");
	   
	  return declaracion;
	 }
	
	
	@ModelAttribute("tipoPeriodicidadList")
	 public Map<String,String> populateTipoPeriodicidad() {
	   
	  //Data referencing for java skills list box
	  Map<String,String> periodicidad = new LinkedHashMap<String,String>();
	  periodicidad.put("mensual", "Mensual");
	  periodicidad.put("bimestral", "Bimestral");
	  periodicidad.put("Trimestral", "Trimestral");
	  periodicidad.put("Cuatrimestral", "Cuatrimestral");
	  periodicidad.put("Semestral","Semestral");
	   
	  return periodicidad;
	 }
	
	
	@RequestMapping(value="/comprobante.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Totales totales,@ModelAttribute Receptor receptor,
			BindingResult result,@RequestParam String action,Map<String,Object> map){
		
		System.out.println("Entra Comprobante ");
		ObjectFactory factory = new ObjectFactory();
		
		DoctoDigital docDigital = factory.createDoctoDigital();
		DoctoDigital.Emisor emisor = factory.createDoctoDigitalEmisor();
		DoctoDigital.Emisor.EDomicilio domicilioEmisor = factory.createDoctoDigitalEmisorEDomicilio();
		CPais cPais = null;
		DoctoDigital.Emisor.EEmpleadoDe empleadoDe= factory.createDoctoDigitalEmisorEEmpleadoDe();
		DoctoDigital.Emisor.ERepresentanteLegal representanteLegal = factory.createDoctoDigitalEmisorERepresentanteLegal();
		DoctoDigital.Receptor receptorE = factory.createDoctoDigitalReceptor();
		
		receptorE.setRApellMat("Marinez");
		receptorE.setRApellPat("Kali");
		receptorE.setRCorreoE("");
		
		empleadoDe.setEDenORazSocOrg("Razon Social SA de CV");
		empleadoDe.setERFCOrg("OOO010101TFG");
		
		representanteLegal.setEApellMatRep("Martinez");
		representanteLegal.setEApellPatRep("Rodrigez");
		representanteLegal.setECURPRep("ARDF121212TFG12");
		representanteLegal.setENombreRep("Ernesto");
		representanteLegal.setERFCRep("01010101");
		
		
		domicilioEmisor.setECalle("Calle interior");
		domicilioEmisor.setEColonia("Colonia Emisor");
		domicilioEmisor.setECP("12345");
		domicilioEmisor.setEEntidadF("Toluca");
		domicilioEmisor.setELocalidad("Toluca");
		domicilioEmisor.setEMunDel("112");
		domicilioEmisor.setENumExt("124");
		domicilioEmisor.setENumInt("1111");
		domicilioEmisor.setEPais(cPais.AF);
		domicilioEmisor.setERef("Referencia");
		
		
		
		emisor.setEDomicilio(domicilioEmisor);
		emisor.setEApellMat("Apellido Mat");
		emisor.setEApellPat("ApellidoPat");
		emisor.setECorreoE("correo@hotmail.com");
		emisor.setECURP("AAAA010101TF33");
		emisor.setEDenORazSoc("Raz�n social SD");
		emisor.setENombre("Pedro");
		emisor.setENumTel("7121213122");
		emisor.setERFC("AAA010101AAA");
		emisor.setERepresentanteLegal(representanteLegal);
		emisor.setEEmpleadoDe(empleadoDe);
		
		TipoDoctoDigital tipoDoctoDigital = factory.createDoctoDigitalTipoDoctoDigital();
		//tipoDoctoDigital.getAny().add(docDigital);
		
		
		
		docDigital.setCert("AwjSddDFjkkhksdss");
		docDigital.setEmisor(emisor);
		docDigital.setFirma("");
		docDigital.setNumCert("012449687545433111");
		docDigital.setReceptor(receptorE);
		docDigital.setTipoDoctoDigitalN(tipoDoctoDigital);
		docDigital.setVersion("1.1");
		
		StringWriter strWriter;
		strWriter = new StringWriter();
		
		JAXBContext context;
		
		try {
			context = JAXBContext.newInstance(DoctoDigital.class);
			Marshaller m = context.createMarshaller();
	        
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	        m.marshal(docDigital, strWriter);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return "page";
	}
		
	
	

}
