package com.quadrum.dpiva.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quadrum.dpiva.dao.EmisorDao;
import com.quadrum.dpiva.dao.ReceptorDao;
import com.quadrum.dpiva.model.Receptor;
import com.quadrum.dpiva.service.ReceptorService;

@Service
public class ReceptorServiceImpl implements ReceptorService {
	
	@Autowired
	private ReceptorDao receptorDao;

	@Transactional
	public void agregar(Receptor receptor) {
		receptorDao.add(receptor);
	}

	@Transactional
	public void actualizar(Receptor receptor) {
		receptorDao.edit(receptor);
	}

	@Transactional
	public void eliminar(int idReceptor) {
		receptorDao.delete(idReceptor);
	}

	@Transactional
	public Receptor traerReceptor(int idReceptor) {
		return receptorDao.getReceptor(idReceptor);
	}

	@Transactional
	public List traerTodosReceptores() {
		return receptorDao.getAllReceptor();
	}

	@Transactional
	public Receptor traeReceptorPorRfc(String rrfc) {	
		return receptorDao.getReceptorRfc(rrfc);
	}
	
	
	

}
