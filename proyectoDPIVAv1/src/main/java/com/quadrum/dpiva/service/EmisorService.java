package com.quadrum.dpiva.service;

import java.util.List;

import com.quadrum.dpiva.model.Emisor;

public interface EmisorService {
	public void agregar(Emisor emisor);
	
	public void actualizar(Emisor emisor);
	
	public void eliminar(int idEmisor);
	
	public Emisor traerEmisor(int idEmisor);
	
	public List traerTodosEmisores();
	
	public Emisor buscaPorRfcEmisor(String rfc); 

}
