package com.quadrum.dpiva.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quadrum.dpiva.dao.EmisorDao;
import com.quadrum.dpiva.model.Emisor;
import com.quadrum.dpiva.service.EmisorService;

@Service
public class EmisorServiceImpl implements EmisorService {

	@Autowired
	private EmisorDao emisorDao;
	
	@Transactional
	public void agregar(Emisor emisor) {
		emisorDao.add(emisor);
	}

	@Transactional
	public void actualizar(Emisor emisor) {
		emisorDao.edit(emisor);
	}

	@Transactional
	public void eliminar(int idEmisor) {
		emisorDao.delete(idEmisor);
	}

	@Transactional
	public Emisor traerEmisor(int idEmisor) {
		return emisorDao.getEmisor(idEmisor);
	}

	@Transactional
	public List traerTodosEmisores() {
		return emisorDao.getAllEmisor();
	}

	@Transactional
	public Emisor buscaPorRfcEmisor(String rfc) {
		return emisorDao.buscaPorRFc(rfc);
	}
	
	

}
