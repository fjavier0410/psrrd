package com.quadrum.dpiva.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quadrum.dpiva.dao.UsuarioDao;
import com.quadrum.dpiva.model.Role;
import com.quadrum.dpiva.model.Usuario;
import com.quadrum.dpiva.model.UsuarioEstatus;

@Service("userDetailsService")
public class UsuarioDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioDao userDao;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Usuario user = userDao.findNombreUsuario(username); //our own User model class
		
		if(user!=null){
			String password = user.getPass();
			//additional information on the security object
			boolean enabled = user.getStatus().equals(UsuarioEstatus.ACTIVE.toString());
			boolean accountNonExpired = user.getStatus().equals(UsuarioEstatus.ACTIVE.toString());
			boolean credentialsNonExpired = user.getStatus().equals(UsuarioEstatus.ACTIVE.toString());
			boolean accountNonLocked = user.getStatus().equals(UsuarioEstatus.ACTIVE.toString());
			
			//Let's populate user roles
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			for(Role role : user.getRoles()){
				authorities.add(new GrantedAuthorityImpl(role.getNombreRole()));
			}
			
			//Now let's create Spring Security User object
			org.springframework.security.core.userdetails.User securityUser = new 
					org.springframework.security.core.userdetails.User(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
			return securityUser;
		}else{
			throw new UsernameNotFoundException("User Not Found!!!");
		}
	}

}
