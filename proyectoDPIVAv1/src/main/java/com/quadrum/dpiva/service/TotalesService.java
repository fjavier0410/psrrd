package com.quadrum.dpiva.service;

import java.util.List;

import com.quadrum.dpiva.model.Totales;

public interface TotalesService {
	
	public void agregarTotales(Totales total);
	
	public void actualizarTotales(Totales total);
	
	public void eliminarTotales(int idTotales);
	
	public Totales traerTotal(int totales);
	
	public List<Totales> traerTodosTotales();

}
