package com.quadrum.dpiva.service;

import com.quadrum.dpiva.model.Usuario;

public interface UsuarioService {

	
	public void editaUsuario(Usuario usuario); 
	
	public Usuario buscaPorId(int id); 
	
	public Usuario buscaPorRFC(String rfc); 
	
}
