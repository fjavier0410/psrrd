package com.quadrum.dpiva.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quadrum.dpiva.dao.TotalesDao;
import com.quadrum.dpiva.model.Totales;
import com.quadrum.dpiva.service.TotalesService;

@Repository
public class TotalesServiceImpl implements TotalesService {
	
	@Autowired
	private TotalesDao totalesDao;

	@Override
	public void agregarTotales(Totales total) {
		totalesDao.addTotales(total);
	}

	@Override
	public void actualizarTotales(Totales total) {
		totalesDao.editTotales(total);
	}

	@Override
	public void eliminarTotales(int idTotales) {
		totalesDao.deleteTotales(idTotales);

	}

	@Override
	public Totales traerTotal(int totales) {
		return totalesDao.findTotales(totales);
	}

	@Override
	public List<Totales> traerTodosTotales() {
		return totalesDao.getAllTotales();
	}

}
