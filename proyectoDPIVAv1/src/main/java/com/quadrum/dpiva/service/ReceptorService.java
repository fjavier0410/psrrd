package com.quadrum.dpiva.service;

import java.util.List;

import com.quadrum.dpiva.model.Receptor;

public interface ReceptorService {
	public void agregar(Receptor receptor);

	public void actualizar(Receptor receptor);

	public void eliminar(int idReceptor);

	public Receptor traerReceptor(int idReceptor);

	public List traerTodosReceptores();
	
	public Receptor traeReceptorPorRfc(String rrfc);

}
