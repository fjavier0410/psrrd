package com.quadrum.dpiva.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quadrum.dpiva.dao.EmisorDao;
import com.quadrum.dpiva.dao.UsuarioDao;
import com.quadrum.dpiva.model.Usuario;
import com.quadrum.dpiva.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;
	@Transactional
	public Usuario buscaPorId(int id) {
		// TODO Auto-generated method stub
	  return usuarioDao.findUsuario(id); 
	}
	@Transactional
	public void editaUsuario(Usuario usuario) {
		usuarioDao.editUsuario(usuario);
		
	}
	@Transactional
	public Usuario buscaPorRFC(String rfc) {
		return usuarioDao.findNombreUsuario(rfc); 
		
	}
	

}
