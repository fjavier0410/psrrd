package com.quadrum.dpiva.wrapper;


import java.util.Date;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import lombok.Data;

@Data

public class DatosGeneralesWrapper {
	private String operacionesTerceros;
	private String tipoDeclaracion;
	private String folioAnterior;
	private Date fechaPresentacionAnterior;
	private String tipoPeriodicidad;
	private String periodoMensual;
	private String periodoBimestral;
	private String periodoTrimestrasl;
	private String periodoCuatrimestral;
	private String periodoSemestral;

}
