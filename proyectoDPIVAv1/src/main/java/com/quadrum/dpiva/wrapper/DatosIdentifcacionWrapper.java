package com.quadrum.dpiva.wrapper;

import lombok.Data;


@Data
public class DatosIdentifcacionWrapper {
	private String rfc;
	private String curp;
	private int ejercicio;
	private String aPaterno;
	private String aMaterno;
	private String razonSocial;
	private String nombre;	

}
