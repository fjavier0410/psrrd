-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: docdpiva
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `documentodigital`
--

DROP TABLE IF EXISTS `documentodigital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentodigital` (
  `idDOCUMENTODIGITAL` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(45) DEFAULT NULL,
  `tipodedocumentodigital` varchar(45) DEFAULT NULL,
  `cert` varchar(45) DEFAULT NULL,
  `numcert` varchar(45) DEFAULT NULL,
  `firma` varchar(45) DEFAULT NULL,
  `EMISOR_idEMISOR` int(11) NOT NULL,
  `RECEPTOR_idRECEPTOR` int(11) NOT NULL,
  `DPIVA_idDPIVA` int(11) NOT NULL,
  PRIMARY KEY (`idDOCUMENTODIGITAL`),
  KEY `fk_DOCUMENTODIGITAL_EMISOR_idx` (`EMISOR_idEMISOR`),
  KEY `fk_DOCUMENTODIGITAL_RECEPTOR1_idx` (`RECEPTOR_idRECEPTOR`),
  KEY `fk_DOCUMENTODIGITAL_DPIVA1_idx` (`DPIVA_idDPIVA`),
  CONSTRAINT `fk_DOCUMENTODIGITAL_DPIVA1` FOREIGN KEY (`DPIVA_idDPIVA`) REFERENCES `mydb`.`dpiva` (`idDPIVA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_DOCUMENTODIGITAL_EMISOR` FOREIGN KEY (`EMISOR_idEMISOR`) REFERENCES `mydb`.`emisor` (`idEMISOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_DOCUMENTODIGITAL_RECEPTOR1` FOREIGN KEY (`RECEPTOR_idRECEPTOR`) REFERENCES `mydb`.`receptor` (`idRECEPTOR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentodigital`
--

LOCK TABLES `documentodigital` WRITE;
/*!40000 ALTER TABLE `documentodigital` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentodigital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dpiva`
--

DROP TABLE IF EXISTS `dpiva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dpiva` (
  `idDPIVA` int(11) NOT NULL AUTO_INCREMENT,
  `Version` varchar(45) DEFAULT NULL,
  `tipoinfo` varchar(45) DEFAULT NULL,
  `folioant` varchar(45) DEFAULT NULL,
  `fechpresant` date DEFAULT NULL,
  PRIMARY KEY (`idDPIVA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dpiva`
--

LOCK TABLES `dpiva` WRITE;
/*!40000 ALTER TABLE `dpiva` DISABLE KEYS */;
/*!40000 ALTER TABLE `dpiva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edomicilio`
--

DROP TABLE IF EXISTS `edomicilio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edomicilio` (
  `idEDOMICILIO` int(11) NOT NULL,
  `ecalle` varchar(45) DEFAULT NULL,
  `enumext` varchar(45) DEFAULT NULL,
  `enumint` varchar(45) DEFAULT NULL,
  `ecolonia` varchar(45) DEFAULT NULL,
  `elocalidad` varchar(45) DEFAULT NULL,
  `eref` varchar(45) DEFAULT NULL,
  `enumdel` varchar(45) DEFAULT NULL,
  `eentidadf` varchar(45) DEFAULT NULL,
  `emundel` varchar(45) DEFAULT NULL,
  `epais` varchar(45) DEFAULT NULL,
  `ecp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEDOMICILIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edomicilio`
--

LOCK TABLES `edomicilio` WRITE;
/*!40000 ALTER TABLE `edomicilio` DISABLE KEYS */;
/*!40000 ALTER TABLE `edomicilio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eempleadode`
--

DROP TABLE IF EXISTS `eempleadode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eempleadode` (
  `idEEMPLEADODE` int(11) NOT NULL AUTO_INCREMENT,
  `erfcorg` varchar(45) DEFAULT NULL,
  `edenorazsocorg` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEEMPLEADODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eempleadode`
--

LOCK TABLES `eempleadode` WRITE;
/*!40000 ALTER TABLE `eempleadode` DISABLE KEYS */;
/*!40000 ALTER TABLE `eempleadode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emisor`
--

DROP TABLE IF EXISTS `emisor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emisor` (
  `idEMISOR` int(11) NOT NULL AUTO_INCREMENT,
  `erfc` varchar(45) DEFAULT NULL,
  `ecurp` varchar(45) DEFAULT NULL,
  `eapellpat` varchar(45) DEFAULT NULL,
  `eapellmat` varchar(45) DEFAULT NULL,
  `enombre` varchar(45) DEFAULT NULL,
  `edenrazsoc` varchar(45) DEFAULT NULL,
  `ecorreoe` varchar(45) DEFAULT NULL,
  `enumtel` varchar(45) DEFAULT NULL,
  `EEMPLEADODE_idEEMPLEADODE` int(11) DEFAULT NULL,
  `EDOMICILIO_idEDOMICILIO` int(11) DEFAULT NULL,
  `EREPRESENTANTELEGAL_idEREPRESENTANTELEGAL` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEMISOR`),
  KEY `fk_EMISOR_EEMPLEADODE1_idx` (`EEMPLEADODE_idEEMPLEADODE`),
  KEY `fk_EMISOR_EDOMICILIO1_idx` (`EDOMICILIO_idEDOMICILIO`),
  KEY `fk_EMISOR_EREPRESENTANTELEGAL1_idx` (`EREPRESENTANTELEGAL_idEREPRESENTANTELEGAL`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emisor`
--

LOCK TABLES `emisor` WRITE;
/*!40000 ALTER TABLE `emisor` DISABLE KEYS */;
INSERT INTO `emisor` VALUES (5,'SSSS020202SDS','AQW121212TF6',NULL,NULL,'Fernando',NULL,NULL,NULL,0,0,0),(6,'','',NULL,NULL,'',NULL,NULL,NULL,0,0,0),(7,'','',NULL,NULL,'',NULL,NULL,NULL,0,0,0),(8,'AAA010101AAA','AQWE121212TG9',NULL,NULL,'Juan',NULL,NULL,NULL,0,0,0),(9,'','',NULL,NULL,'',NULL,NULL,NULL,0,0,0);
/*!40000 ALTER TABLE `emisor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `erepresentantelegal`
--

DROP TABLE IF EXISTS `erepresentantelegal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `erepresentantelegal` (
  `idEREPRESENTANTELEGAL` int(11) NOT NULL AUTO_INCREMENT,
  `erfcrep` varchar(45) DEFAULT NULL,
  `ecurprep` varchar(45) DEFAULT NULL,
  `eapellpatrep` varchar(45) DEFAULT NULL,
  `eapellmatrep` varchar(45) DEFAULT NULL,
  `enombrerep` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEREPRESENTANTELEGAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `erepresentantelegal`
--

LOCK TABLES `erepresentantelegal` WRITE;
/*!40000 ALTER TABLE `erepresentantelegal` DISABLE KEYS */;
/*!40000 ALTER TABLE `erepresentantelegal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eunidad`
--

DROP TABLE IF EXISTS `eunidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eunidad` (
  `idEUNIDAD` int(11) NOT NULL AUTO_INCREMENT,
  `eunidad` varchar(45) DEFAULT NULL,
  `EEMPLEADODE_idEEMPLEADODE` int(11) NOT NULL,
  PRIMARY KEY (`idEUNIDAD`),
  KEY `fk_EUNIDAD_EEMPLEADODE1_idx` (`EEMPLEADODE_idEEMPLEADODE`),
  CONSTRAINT `fk_EUNIDAD_EEMPLEADODE1` FOREIGN KEY (`EEMPLEADODE_idEEMPLEADODE`) REFERENCES `mydb`.`eempleadode` (`idEEMPLEADODE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eunidad`
--

LOCK TABLES `eunidad` WRITE;
/*!40000 ALTER TABLE `eunidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `eunidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provextranjero`
--

DROP TABLE IF EXISTS `provextranjero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provextranjero` (
  `idPROVEXTRANJERO` int(11) NOT NULL AUTO_INCREMENT,
  `TipoOperac` varchar(45) DEFAULT NULL,
  `RFCProv` varchar(45) DEFAULT NULL,
  `NumIDFiscal` varchar(45) DEFAULT NULL,
  `NombExtranjero` varchar(45) DEFAULT NULL,
  `PaisResidencia` varchar(45) DEFAULT NULL,
  `Nacionalidad` varchar(45) DEFAULT NULL,
  `ValActPagTas15o16IVA` varchar(45) DEFAULT NULL,
  `ValActPagTas15IVA` varchar(45) DEFAULT NULL,
  `MonIVAPagNoAcrTas15o16` varchar(45) DEFAULT NULL,
  `ValActPagTas10u11IVA` varchar(45) DEFAULT NULL,
  `ValActPagTas10IVA` varchar(45) DEFAULT NULL,
  `MonIVAPagNoAcrTas10u11` varchar(45) DEFAULT NULL,
  `ValActPagImpBySTas15016IVA` varchar(45) DEFAULT NULL,
  `MonIVAPagNoAcrImpTas15o16` varchar(45) DEFAULT NULL,
  `ValActPagImpBySTas10u11IVA` varchar(45) DEFAULT NULL,
  `MonIVAPagNoAcrImpTas10u11` varchar(45) DEFAULT NULL,
  `ValActPagImpBySNoIVA` varchar(45) DEFAULT NULL,
  `ValActPagTas0IVA` varchar(45) DEFAULT NULL,
  `ValActPagNoIVA` varchar(45) DEFAULT NULL,
  `IVARetCont` varchar(45) DEFAULT NULL,
  `IVADevDescyBonifComp` varchar(45) DEFAULT NULL,
  `DPIVA_idDPIVA` int(11) NOT NULL,
  PRIMARY KEY (`idPROVEXTRANJERO`),
  KEY `fk_PROVEXTRANJERO_DPIVA1_idx` (`DPIVA_idDPIVA`),
  CONSTRAINT `fk_PROVEXTRANJERO_DPIVA1` FOREIGN KEY (`DPIVA_idDPIVA`) REFERENCES `mydb`.`dpiva` (`idDPIVA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provextranjero`
--

LOCK TABLES `provextranjero` WRITE;
/*!40000 ALTER TABLE `provextranjero` DISABLE KEYS */;
/*!40000 ALTER TABLE `provextranjero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provglobal`
--

DROP TABLE IF EXISTS `provglobal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provglobal` (
  `idPROVGLOBAL` int(11) NOT NULL AUTO_INCREMENT,
  `TipoOperac` int(11) DEFAULT NULL,
  `ValActPagTas15o16IVA` int(11) DEFAULT NULL,
  `ValActPagTas15IVA` int(11) DEFAULT NULL,
  `MonIVAPagNoAcrTas15o16` int(11) DEFAULT NULL,
  `ValActPagTas10u11IVA` int(11) DEFAULT NULL,
  `ValActPagTas10IVA` int(11) DEFAULT NULL,
  `MonIVAPagNoAcrTas10u11` int(11) DEFAULT NULL,
  `ValActPagImpBySTas15016IVA` int(11) DEFAULT NULL,
  `MonIVAPagNoAcrImpTas15o16` int(11) DEFAULT NULL,
  `ValActPagImpBySTas10u11IVA` int(11) DEFAULT NULL,
  `MonIVAPagNoAcrImpTas10u11` int(11) DEFAULT NULL,
  `ValActPagImpBySNoIVA` int(11) DEFAULT NULL,
  `ValActPagTas0IVA` int(11) DEFAULT NULL,
  `ValActPagNoIVA` int(11) DEFAULT NULL,
  `IVARetCont` int(11) DEFAULT NULL,
  `IVADevDescyBonifComp` int(11) DEFAULT NULL,
  `DPIVA_idDPIVA` int(11) NOT NULL,
  PRIMARY KEY (`idPROVGLOBAL`),
  KEY `fk_PROVGLOBAL_DPIVA1_idx` (`DPIVA_idDPIVA`),
  CONSTRAINT `fk_PROVGLOBAL_DPIVA1` FOREIGN KEY (`DPIVA_idDPIVA`) REFERENCES `mydb`.`dpiva` (`idDPIVA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provglobal`
--

LOCK TABLES `provglobal` WRITE;
/*!40000 ALTER TABLE `provglobal` DISABLE KEYS */;
/*!40000 ALTER TABLE `provglobal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provnacional`
--

DROP TABLE IF EXISTS `provnacional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provnacional` (
  `idPROVNACIONAL` int(11) NOT NULL AUTO_INCREMENT,
  `tipooperac` varchar(45) DEFAULT NULL COMMENT 'Catalogo tipo de opracion\n',
  `RFCProv` varchar(45) DEFAULT NULL,
  `valactpagtas15o16IVA` int(11) DEFAULT NULL,
  `valactpagtas15IVA` int(11) DEFAULT NULL,
  `monIVApagnoacrtas15o16` int(11) DEFAULT NULL,
  `valactpagtas10u11IVA` int(11) DEFAULT NULL,
  `valactpagtas10IVA` int(11) DEFAULT NULL,
  `monIVApagnoacrtas10u11` int(11) DEFAULT NULL,
  `valactpagtas0IVA` int(11) DEFAULT NULL,
  `valactpagnoIVA` int(11) DEFAULT NULL,
  `IVAretcont` int(11) DEFAULT NULL,
  `IVAdevdescybonifcomp` int(11) DEFAULT NULL,
  `DPIVA_idDPIVA` int(11) NOT NULL,
  PRIMARY KEY (`idPROVNACIONAL`),
  KEY `fk_PROVNACIONAL_DPIVA1_idx` (`DPIVA_idDPIVA`),
  CONSTRAINT `fk_PROVNACIONAL_DPIVA1` FOREIGN KEY (`DPIVA_idDPIVA`) REFERENCES `mydb`.`dpiva` (`idDPIVA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provnacional`
--

LOCK TABLES `provnacional` WRITE;
/*!40000 ALTER TABLE `provnacional` DISABLE KEYS */;
/*!40000 ALTER TABLE `provnacional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rdomicilio`
--

DROP TABLE IF EXISTS `rdomicilio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rdomicilio` (
  `idRDOMICILIO` int(11) NOT NULL,
  `rcalle` varchar(45) DEFAULT NULL,
  `rnumext` varchar(45) DEFAULT NULL,
  `rnumint` varchar(45) DEFAULT NULL,
  `rcolonia` varchar(45) DEFAULT NULL,
  `rlocalidad` varchar(45) DEFAULT NULL,
  `rref` varchar(45) DEFAULT NULL,
  `rnumdel` varchar(45) DEFAULT NULL,
  `rentidadf` varchar(45) DEFAULT NULL,
  `rmundel` varchar(45) DEFAULT NULL,
  `rpais` varchar(45) DEFAULT NULL,
  `rcp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRDOMICILIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rdomicilio`
--

LOCK TABLES `rdomicilio` WRITE;
/*!40000 ALTER TABLE `rdomicilio` DISABLE KEYS */;
/*!40000 ALTER TABLE `rdomicilio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptor`
--

DROP TABLE IF EXISTS `receptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receptor` (
  `idRECEPTOR` int(11) NOT NULL AUTO_INCREMENT,
  `rrfc` varchar(45) DEFAULT NULL,
  `rcurp` varchar(45) DEFAULT NULL,
  `rapellpat` varchar(45) DEFAULT NULL,
  `rapellmat` varchar(45) DEFAULT NULL,
  `rnombre` varchar(45) DEFAULT NULL,
  `rdenrazsoc` varchar(45) DEFAULT NULL,
  `rcorreoe` varchar(45) DEFAULT NULL,
  `rnumtel` varchar(45) DEFAULT NULL,
  `REMPLEADODE_idREMPLEADODE` int(11) DEFAULT NULL,
  `RDOMICILIO_idRDOMICILIO` int(11) DEFAULT NULL,
  `RREPRESENTANTELEGAL_idRREPRESENTANTELEGAL` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRECEPTOR`),
  KEY `fk_RECEPTOR_REMPLEADODE1_idx` (`REMPLEADODE_idREMPLEADODE`),
  KEY `fk_RECEPTOR_RDOMICILIO1_idx` (`RDOMICILIO_idRDOMICILIO`),
  KEY `fk_RECEPTOR_RREPRESENTANTELEGAL1_idx` (`RREPRESENTANTELEGAL_idRREPRESENTANTELEGAL`),
  CONSTRAINT `fk_RECEPTOR_RDOMICILIO1` FOREIGN KEY (`RDOMICILIO_idRDOMICILIO`) REFERENCES `mydb`.`rdomicilio` (`idRDOMICILIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_RECEPTOR_REMPLEADODE1` FOREIGN KEY (`REMPLEADODE_idREMPLEADODE`) REFERENCES `mydb`.`rempleadode` (`idREMPLEADODE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_RECEPTOR_RREPRESENTANTELEGAL1` FOREIGN KEY (`RREPRESENTANTELEGAL_idRREPRESENTANTELEGAL`) REFERENCES `mydb`.`rrepresentantelegal` (`idRREPRESENTANTELEGAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptor`
--

LOCK TABLES `receptor` WRITE;
/*!40000 ALTER TABLE `receptor` DISABLE KEYS */;
INSERT INTO `receptor` VALUES (1,NULL,NULL,'denova','barrutea','pedro',NULL,NULL,NULL,NULL,NULL,NULL),(2,NULL,NULL,'denova','barrueta','pedro',NULL,NULL,NULL,NULL,NULL,NULL),(4,'OOO121212OPT','WERT121212OPY6','Vargas','','Wilfrido','Vargas','vargas@gmail.com','55521238787',NULL,NULL,NULL),(5,'OOO121212OPT','WERT121212OPY6','Vargas','','Wilfrido','Vargas','vargas@gmail.com','55521238787',NULL,NULL,NULL),(6,'IYU040404FGD','FGHJ787878H&7YF','Orozco','Minde','Juan','Minde','minde@outlook.com','777856963123',NULL,NULL,NULL);
/*!40000 ALTER TABLE `receptor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rempleadode`
--

DROP TABLE IF EXISTS `rempleadode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rempleadode` (
  `idREMPLEADODE` int(11) NOT NULL AUTO_INCREMENT,
  `rrfcorg` varchar(45) DEFAULT NULL,
  `rdenorazsocorg` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idREMPLEADODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rempleadode`
--

LOCK TABLES `rempleadode` WRITE;
/*!40000 ALTER TABLE `rempleadode` DISABLE KEYS */;
/*!40000 ALTER TABLE `rempleadode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rrepresentantelegal`
--

DROP TABLE IF EXISTS `rrepresentantelegal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rrepresentantelegal` (
  `idRREPRESENTANTELEGAL` int(11) NOT NULL AUTO_INCREMENT,
  `rrfcrep` varchar(45) DEFAULT NULL,
  `rcurprep` varchar(45) DEFAULT NULL,
  `rapellpatrep` varchar(45) DEFAULT NULL,
  `rapellmatrep` varchar(45) DEFAULT NULL,
  `rnombrerep` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRREPRESENTANTELEGAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rrepresentantelegal`
--

LOCK TABLES `rrepresentantelegal` WRITE;
/*!40000 ALTER TABLE `rrepresentantelegal` DISABLE KEYS */;
/*!40000 ALTER TABLE `rrepresentantelegal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `runidadorg`
--

DROP TABLE IF EXISTS `runidadorg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runidadorg` (
  `idRUNIDADORG` int(11) NOT NULL AUTO_INCREMENT,
  `runidad` varchar(45) DEFAULT NULL,
  `REMPLEADODE_idREMPLEADODE` int(11) NOT NULL,
  PRIMARY KEY (`idRUNIDADORG`),
  KEY `fk_RUNIDADORG_REMPLEADODE1_idx` (`REMPLEADODE_idREMPLEADODE`),
  CONSTRAINT `fk_RUNIDADORG_REMPLEADODE1` FOREIGN KEY (`REMPLEADODE_idREMPLEADODE`) REFERENCES `mydb`.`rempleadode` (`idREMPLEADODE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `runidadorg`
--

LOCK TABLES `runidadorg` WRITE;
/*!40000 ALTER TABLE `runidadorg` DISABLE KEYS */;
/*!40000 ALTER TABLE `runidadorg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `totales`
--

DROP TABLE IF EXISTS `totales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `totales` (
  `idTOTALES` int(11) NOT NULL AUTO_INCREMENT,
  `totoperac` int(11) DEFAULT NULL,
  `totactpagtas15o16IVA` int(11) DEFAULT NULL,
  `totactpagtas15IVA` int(11) DEFAULT NULL,
  `totIVApagnoacrtas15o16` int(11) DEFAULT NULL,
  `totactpagtas10u11IVA` int(11) DEFAULT NULL,
  `totactpagtas10IVA` int(11) DEFAULT NULL,
  `totIVApagnoacrtas10u11` int(11) DEFAULT NULL,
  `totactpagimpbystas15o16IVA` int(11) DEFAULT NULL,
  `totIVApagnoacrimptas15o16` int(11) DEFAULT NULL,
  `totactpagimpbystas10u11IVA` int(11) DEFAULT NULL,
  `totIVApagnoacrimptas10u11` int(11) DEFAULT NULL,
  `totactpagimpbysnopagIVA` int(11) DEFAULT NULL,
  `totdemactpagtas0IVA` int(11) DEFAULT NULL,
  `totactpagnopagIVA` int(11) DEFAULT NULL,
  `totIVAretcont` int(11) DEFAULT NULL,
  `totIVAdevdescybonifcomp` int(11) DEFAULT NULL,
  `totIVAtraslcontexcimpByS` int(11) DEFAULT NULL,
  `totIVApagimpByS` int(11) DEFAULT NULL,
  `DPIVA_idDPIVA` int(11) NOT NULL,
  PRIMARY KEY (`idTOTALES`),
  KEY `fk_TOTALES_DPIVA1_idx` (`DPIVA_idDPIVA`),
  CONSTRAINT `fk_TOTALES_DPIVA1` FOREIGN KEY (`DPIVA_idDPIVA`) REFERENCES `mydb`.`dpiva` (`idDPIVA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `totales`
--

LOCK TABLES `totales` WRITE;
/*!40000 ALTER TABLE `totales` DISABLE KEYS */;
/*!40000 ALTER TABLE `totales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nom_usuario` varchar(45) DEFAULT NULL,
  `pass` varchar(145) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'pedro','c6cc8094c2dc07b700ffcc36d64e2138','ACTIVE'),(2,'jose','662eaa47199461d01a623884080934ab','ACTIVE');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_y_role`
--

DROP TABLE IF EXISTS `usuario_y_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_y_role` (
  `idusuario_y_role` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusuario_y_role`),
  KEY `id_usuario_idx` (`usuario_id`),
  KEY `id_role_idx` (`role_id`),
  CONSTRAINT `id_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_y_role`
--

LOCK TABLES `usuario_y_role` WRITE;
/*!40000 ALTER TABLE `usuario_y_role` DISABLE KEYS */;
INSERT INTO `usuario_y_role` VALUES (1,1,1),(2,1,2),(3,2,2);
/*!40000 ALTER TABLE `usuario_y_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-20 13:56:40
